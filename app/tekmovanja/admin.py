from django.contrib import admin
from cryptography.fernet import Fernet, InvalidToken
from django.core.exceptions import ValidationError
from tekmovanja.lib.send_mail_to_user import *
from django import forms
from django.forms import PasswordInput

# Register your models here.
from django.contrib import admin
from .models import *
admin.site.site_header='Administrator'
admin.site.register(Verifikacija)
admin.site.register(Pozadbljenogesloverifikacija)
admin.site.register(Sola)
admin.site.register(Potrditve)
admin.site.register(Uci)
admin.site.register(Tekmovanje)
admin.site.register(Sodeluje)
admin.site.register(email_merge)
admin.site.register(Skritatekmovanja)
admin.site.register(user_help)
admin.site.register(Messages)
admin.site.register(Mail_settings)
admin.site.register(mail_group)
admin.site.register(mail_user_groups)

class MyModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MyModelForm, self).__init__(*args, **kwargs)
        if 'email_host_password' in self.initial:
            password = self.initial['email_host_password']
            if canDecryptPassword() == False:
                password = ""
            self.initial['email_host_password'] = password
    class Meta:
        model = system_email_settings
        fields=[
            'email_host_password'
        ]
        widgets = {
            'email_host_password': forms.PasswordInput(render_value=True),
        }

class system_email_settingsAdmin(admin.ModelAdmin):
    list_display = ('get_description',)
    form = MyModelForm

    def get_description(self, obj):
     email_settings = system_email_settings.objects.count()
     if obj.system_email_settings_id == 1:
        return "Nastavitve SMTP strežnika, ki se uporabljajo za pošiljanje pošte v sistemu"
     else:
        return "Izbrišite to nastavitev, ker ne bo uporabljena v sistemu. Uporabljena bo le nastavitev ki ste jo dodali preko admin vmesnika (Gumb: Add system_email_settings). Dodajte nastavitev preko vmesnika ali uredite obstoječo."

    def get_fieldsets(self, request, obj):
        description = ""
        if obj != None:
            if canDecryptPassword():
                description = """
                 V vnosnem polju je kripritano geslo (zato je dolžina verjetno daljša od dolžine, ki ste jo vnesli).
                 Geslo lahko zamenjate tako, da izbrišete geslo in vnesete novega. Ob shranitvi se bo geslo ustrezno zakriptiralo.
                 Vnešeno geslo lahko preverite tako, da preko zgornjega vmesnika vnesete SMTP geslo. Če se bo geslo ujema z shranjenim nam bo vmesnik vrnil ujemanje, sicer nam bo vrnil neujemanje.
                 Nekriptirano geslo moramo pridobiti ročno saj si v vmesniku ne želimo prikazovati nekriptiranega gesla (preko vrednosti SMTP_PASSWORD_SECRET_KEY v konf. dat. in kriptiranega gesla). Geslo se kriptira s pomočjo knjižnice FERNET."""
            else:
                description = "Gesla ni mogoče dekriptirati. Če v konfiguracijski datoteki manjka kriptirni ključ ga vnesite. Če ste popravili ali spremenili vrednost ključa ponovno vnesite geslo. Če je vrednost ključa pravilna se bo ob shranitvi vnešeno geslo ponovno zakriptiralo."
        return [
            (None, {'fields': ['email_host']}),
            (None, {'fields': ['email_port']}),
            (None, {'fields': ['email_host_user']}),
            (None, {'fields': ['email_host_password']}),
            (None, {'fields': ['email_from'],
                    'description': description
            }),
            (None, {'fields': ['email_use_tls']}),
        ]
admin.site.body='Administrator'

admin.site.register(system_email_settings, system_email_settingsAdmin)