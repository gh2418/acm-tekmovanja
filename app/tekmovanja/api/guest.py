from django.views.decorators.csrf import csrf_exempt, csrf_protect
from rest_framework.decorators import api_view, permission_classes
from rest_framework.decorators import authentication_classes
from rest_framework_jwt.settings import api_settings
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
from django.contrib.auth import authenticate
from rest_framework.response import Response
from django.http import JsonResponse
from tekmovanja.serializers import *
from django.conf import settings
from site_config import settings as project_settings
from django.contrib import auth
from tekmovanja.lib.send_mail_to_user import send_mail_to_user, get_smtp_settings
import datetime
import random
import string
import re

import os
import datetime
import json

# generetor naključnih končnic url-jev
def randomStringDigits(stringLength=60):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))

#preveri ali se je kdo že registriral vendar še nima aktiven račun.
#če je preteklo več kot n sekund in registrirani uporabnik še ni aktiviral
#računa ga izbriši ter trenutnemu uporabniku, omogoči izbiro up. imena.
#Če se je ob klicu sprostilo up. ime vrni true, sicer false
def deleteUserIfNotVerifiedUsername(username,seconds):
    if User.objects.filter(username=username).exists():
        if User.objects.filter(username=username, is_staff=1).exists():
            return False
        if User.objects.filter(username=username, is_superuser=1).exists():
            return False
        user=User.objects.get(username=username)
        if user.is_active == 0:
            naive = user.date_joined.replace(tzinfo=None)
            seconds_from_registration=datetime.datetime.now().timestamp()-naive.timestamp()
            if seconds_from_registration > seconds:
                Verifikacija.objects.filter(user_id=user.id).delete()
                Uci.objects.filter(ID_Mentor=user.id).delete()
                Potrditve.objects.filter(Zahteva_id=user.id).delete()
                Potrditve.objects.filter(Potrditelj_id=user.id).delete()
                Sodeluje.objects.filter(ID_Mentor=user.id).delete()
                user_email_change.objects.filter(user_id=user.id).delete()
                user.delete()
                return True
            else:
                return False
        else:
            return False
    return False
#preveri ali se je kdo že registriral vendar še nima aktiven račun.
#če je preteklo več kot n sekund in registrirani uporabnik še ni aktiviral
#računa ga izbriši ter trenutnemu uporabniku, omogoči izbiro email naslova.
#Če se je ob klicu sprostil email naslov vrni true, sicer false
def deleteUserIfNotVerifiedEmail(email,seconds):
    if User.objects.filter(email=email).exists():
        if User.objects.filter(email=email, is_staff=1).exists():
            return False
        if User.objects.filter(email=email, is_superuser=1).exists():
            return False
        user=User.objects.get(email=email)
        if user.is_active == 0:
            naive = user.date_joined.replace(tzinfo=None)
            seconds_from_registration=datetime.datetime.now().timestamp()-naive.timestamp()
            if seconds_from_registration > seconds:
                Verifikacija.objects.filter(user_id=user.id).delete()
                Uci.objects.filter(ID_Mentor=user.id).delete()
                Potrditve.objects.filter(Zahteva_id=user.id).delete()
                Potrditve.objects.filter(Potrditelj_id=user.id).delete()
                Sodeluje.objects.filter(ID_Mentor=user.id).delete()
                user_email_change.objects.filter(user_id=user.id).delete()
                user.delete()
                return True
            else:
                return False
        else:
            return False
    return False

#...........................................................
# API-ji ZA GOSTA
#...........................................................

@api_view(['POST'])
@authentication_classes([])
def registration(request):
    username = request.data['UpIme']
    email = request.data['email']
    password = request.data['geslo']
    first_name = request.data['Ime']
    last_name = request.data['Priimek']
    izbrane_sole = request.data['izbrane_sole']
    pogoji_uporabe = request.data['pogoji_uporabe']
    if not isinstance(izbrane_sole,list):
        return Response("schools not in list", status=409)

    if not pogoji_uporabe:
        return Response("pogoji_uporabe", 400)
    
    if username != username.strip():
        return Response("up_ime_vsebuje_presledke",status=400)

    #Prevent whitespace, only one @
    if bool(re.search('^[^@\s]+@[^@\s]+[^@\s]+$', email)) == False:
        return Response("neveljaven_elektronski_naslov",status=400)

    if 'AnonymousUser' in username:
        return Response(status=407)

    if len(username) > 40 or len(email) > 40 or len(password) > 40:
        return Response(status=409)

    if len(password) < 5:
        return Response("geslo_premalo_znakov", status=400)

    if User.objects.filter(username=username).exists():
        if(deleteUserIfNotVerifiedUsername(username,3600) == False):
            return Response("up_ime_zasedeno",status=400)

    if User.objects.filter(email=email).exists():
        if(deleteUserIfNotVerifiedEmail(email,3600) == False):
            return Response("email_zaseden",status=400)

    #check if some other user is changing his email and not confirmed email yet
    if user_email_change.objects.filter(new_email=email).exists():
        token = user_email_change.objects.get(new_email=email)    
        naive = token.created_at.replace(tzinfo=None)
        seconds_from_request=datetime.datetime.now().timestamp()-naive.timestamp()
        if seconds_from_request > 3600:
            user_email_change.objects.filter(new_email=email).delete()
        else:
            return Response("email_zaseden",status=400)
            
    user = User.objects.create_user(
        first_name  = first_name,
        last_name   = last_name,
        username    = username,
        email       = email,
        password    = password,
        is_active   = False
    )

    #dodaj šole mentorju, ki se jih je izbral ob registraciji
    for sola in izbrane_sole:
        sola=Sola.objects.filter(Naziv=sola)[0]
        u = Uci(Potrjen=0,ID_Mentor=user,ID_Sola=sola)
        u.save()
    
    rand = randomStringDigits(random.randrange(50, 60))
    while(Verifikacija.objects.filter(token=rand).exists()):
        rand = randomStringDigits(random.randrange(50, 60))
    token = Verifikacija.objects.create(user=user,token=rand)
    
    #pošiljanje elektronskega sporočila
    message="""
    Pozdravljeni. 
    
    V spletno aplikacijo ACM tekmovanja ste se uspešno registrirali. 
    
    Prijava v sistem bo mogoča po potrditvi elektronskega računa.
    
    Elektronski račun potrdite s klikom na naslednji url naslov: """+project_settings.BASE_URL+"""/verifikacija/"""+str(token.token) + """
    
    Ekipa ACM
    """
    
    sendSuccess = send_mail_to_user("ACM mail verification", message, [user.email], False)

    if sendSuccess:
        return Response(status=201)
    else:
        return Response("no_mail_sent", status=400)


@api_view(['POST'])
@authentication_classes([])
def create_login_phone(request):
    try:
        username = request.data['username']
        password = request.data['password']
    except:
        return Response(status=400)
    if username == '' or password == '':
        return Response(status=409)
    if User.objects.filter(username=username, is_active=0):
        return Response(status=400)
    
    if User.objects.filter(username=username, is_superuser=1, is_staff=1).exists():
        return Response(status=400)

    user = authenticate(username=username, password=password)

    if user is not None:
        serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )})
        serializer.is_valid()
        return Response(serializer.data)
    else:
        return Response(status=400)

@api_view(['GET'])
@authentication_classes([])
def get_seznam_naziv_sol(requst):
    sole=Sola.objects.all().values("Naziv")
    sole_list = list()
    for s in sole:
        sole_list.append(s["Naziv"])
    return JsonResponse(sole_list,safe=False)


@api_view(['GET'])
@authentication_classes([])
def get_public_schools(request):
    data=list(Sola.objects.all().values("Naziv"))
    return JsonResponse(data,safe=False)

@api_view(['POST'])
def send_lost_password_email(request):
    email = request.data['email']

    if not User.objects.filter(email=email,is_superuser=0,is_staff=0).exists():
        return Response(status=200)

    user = User.objects.get(email=email)

    if Pozadbljenogesloverifikacija.objects.filter(user_id=user.id).exists():
        Pozadbljenogesloverifikacija.objects.filter(user_id=user.id).delete()

    rand = randomStringDigits(random.randrange(40, 60))
    while(Pozadbljenogesloverifikacija.objects.filter(token=rand).exists()):
        rand = randomStringDigits(random.randrange(40, 60))

    token = Pozadbljenogesloverifikacija.objects.create(user=user,token=rand)

    #pošiljanje elektronskega sporočila
    message="""
    Pozdravljeni.
    
    V spletni aplikaciji ACM tekmovanja ste zaprosili za zamenjavo gesla. 
    Za uspešno zamenjavo kliknite na naslednji url naslov: """+project_settings.BASE_URL+"""/lost_password_verify/"""+str(token.token)+"""
    
    V kolikor niste vi zahtevali ponastavitve gesla to sporočilo ignorirajte.
    

    Ekipa ACM
    """

    send_mail_to_user('ACM lost password', message, [email], False)
    return Response(status=200)

@api_view(['POST'])
def send_lost_password_token(request):
    try:
        token=request.data['token']
        password=request.data['password']
        repeatPassword=request.data['repeatPassword']
    except:
        return Response("missing data",status=400)

    if not Pozadbljenogesloverifikacija.objects.filter(token=token).exists():
        return Response(status=404)

    token = Pozadbljenogesloverifikacija.objects.get(token=token)
    naive = token.created_at.replace(tzinfo=None)
    seconds_from_request=datetime.datetime.now().timestamp()-naive.timestamp()
    usr = User.objects.get(id=token.user_id)

    if not User.objects.filter(email=usr.email,is_superuser=0,is_staff=0).exists():
        Pozadbljenogesloverifikacija.objects.filter(user_id=usr.id).delete()
        return Response(status=403)

    if(seconds_from_request > 900):
        Pozadbljenogesloverifikacija.objects.filter(user_id=usr.id).delete()
        return Response("token potekel",status=400)
    else:

        if password != repeatPassword:
            return Response("passwordNotMatching", status=400)

        if len(password) < 5:
            return Response("passwordTooLowChars", status=400)

        usr.set_password(password)
        usr.save()
        Pozadbljenogesloverifikacija.objects.filter(user_id=usr.id).delete()
        
        #pošijanje elektronskega sporočila
        message="""
        Pozdravljeni. 
        
        V spletni aplikaciji ACM tekmovanja ("""+ project_settings.BASE_URL +""") ste za uporabniško ime: """+ usr.username +""" uspešno zamenjali geslo.

        Ekipa ACM
        """

        send_mail_to_user('ACM new password', message, [usr.email], False)
        return Response("Geslo zamenjano")

