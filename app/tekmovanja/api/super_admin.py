from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from rest_framework.permissions import BasePermission
from django.contrib.auth.models import User, Group
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from tekmovanja.lib.send_mail_to_user import *
from django.conf import settings
from site_config import settings as project_settings
from rest_framework import permissions
from tekmovanja.serializers import *
from rest_framework import viewsets
from tekmovanja.models import *
import datetime
import string
import random
import time

import os
import datetime
import json

# generetor naključnih končnic url-jev
def randomStringDigits(stringLength=60):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))

#...........................................................
# API-ji ZA SUPERADMINA
#...........................................................
class IsSuperUser(BasePermission):
    """
    Allows access only to admin users.
    """
    def has_permission(self, request, view):
        return request.user.is_superuser

class SolaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Sola.objects.all()
    serializer_class = SolaSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class Mail_settingsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Mail_settings.objects.all()
    serializer_class = Mail_settingsSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class MentorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class PotrditveViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Potrditve.objects.all()
    serializer_class = PotrditveSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class UciViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Uci.objects.all()
    serializer_class = UciSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class TekmovanjeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Tekmovanje.objects.all()
    serializer_class = TekmovanjeSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class Mail_groupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = mail_group.objects.all()
    serializer_class = Mail_groupSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class SodelujeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Sodeluje.objects.all()
    serializer_class = SodelujeSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

class Mail_user_groupsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = mail_user_groups.objects.all()
    serializer_class = Mail_user_groupsSerializer
    permission_classes = [IsAuthenticated, IsSuperUser]

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsSuperUser))
def bulk_users(request):
    try:
        users = request.data["users"]
    except:
        return Response("no_required_parameters",status=400)
    if len(users) == 0:
        return Response("no_data",status=400)
    list_of_emails = list()
    list_of_usernames= list()
    for user in users:
        if len(user) == 4:
            username=user[2]
            email=user[3]
            if "@" in username:
                return Response("up_ime_vsebuje_nedovoljen_znak",status=400)
            if "@" not in email:
                return Response("napacen_email",status=400)
            if User.objects.filter(username=username).exists():
                return Response(username,status=409)
            if User.objects.filter(email=email).exists():
                return Response(email,status=409)

            list_of_emails.append(email)
            list_of_usernames.append(username)
        else:
            if len(user) != 0:
                return Response("data_length_conflict",status=400)
    if len(list_of_emails) != len(set(list_of_emails)) or len(list_of_usernames) != len(set(list_of_usernames)):
        return Response("podvojeni_podatki",status=400)

    for user in users:
        password=randomStringDigits(random.randrange(41, 50))
        first_name=user[0]
        last_name=user[1]
        username=user[2]
        email=user[3]

        u = User()
        u.first_name=first_name
        u.last_name=last_name
        u.username=username
        u.email=email
        u.is_active=1
        u.set_password(password)
        u.save()

        rand = randomStringDigits(random.randrange(40, 60))
        while(Pozadbljenogesloverifikacija.objects.filter(token=rand).exists()):
            rand = randomStringDigits(random.randrange(40, 60))
        token = Pozadbljenogesloverifikacija.objects.create(user=u,token=rand)

        message="""
        Pozdravljeni.

        V aplikaciji ACM mentorji ste dobili svoj račun. V svoj račun se lahko prijavite z uporabniškim imenom: """+ username +""".

        Geslo si ponastavite na: """+project_settings.BASE_URL+"""/lost_password_verify/"""+str(token.token)+"""
        Veljavnost linka za ponastavitev gesla je 15 minut. V kolikor ne uspete ponastaviti gesla v tem času pojdite na """+project_settings.BASE_URL+"""/lost_password/ """+""" vnesite
        trenutni elektronski naslov in sledite navodilom za ponastavitev gesla.

        Aplikacija je dosegljiva na """+project_settings.BASE_URL+""".

        Ekipa ACM.
        """
        send_mail_to_user('ACM new account', message, [email], False)
    return Response("success",status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsSuperUser))
def merge_users(request):
    try:
        emails = request.data["emails"]
        masterEmail = request.data["masterEmail"]
    except:
        return Response("no_required_parameters",status=400)

    if len(emails) < 0:
        return Response("no_data",status=400)

    if not User.objects.filter(email=masterEmail).exists():
        return Response("email_not_found", status=400)

    if "@" not in masterEmail:
        return Response("napacen_email", status=400)

    for email in list(emails):
        if email != "":
            if "@" not in email:
                return Response("napacen_email", status=400)

            if not User.objects.filter(email=email).exists():
                return Response("email_not_found", status=400)

            if not User.objects.filter(email=email,is_active=1).exists():
                return Response("user_not_confirmed", status=400)

            if User.objects.filter(email=email,is_staff=1).exists():
                return Response("only_users", status=400)

            if User.objects.filter(email=email,is_superuser=1).exists():
                return Response("only_users", status=400)

            #če obstaja že kakšen izpis od prej ga izbriši če je preteklo n sekundđ
            if email_merge.objects.filter(Child_mail=email).exists():
                naive = email_merge.objects.get(Child_mail=email).created_at.replace(tzinfo=None)
                seconds_from_request=datetime.datetime.now().timestamp()-naive.timestamp()
                if seconds_from_request > 21600:
                    email_merge.objects.filter(Child_mail=email).delete()
                else:
                    return Response(email, status=409)

            if email_merge.objects.filter(Master_mail=email).exists():
                naive = email_merge.objects.get(Master_mail=email).created_at.replace(tzinfo=None)
                seconds_from_request=datetime.datetime.now().timestamp()-naive.timestamp()
                if seconds_from_request > 21600:
                    email_merge.objects.filter(Master_mail=email).delete()
                else:
                    return Response(email, status=409)
    
    for email in list(emails):
        if email != "":
            rand = randomStringDigits(random.randrange(50, 60))
            while(email_merge.objects.filter(token=rand).exists()):
                rand = randomStringDigits(random.randrange(50, 60))
            message="""Pozdravljeni. V aplikaciji ACM mentorji ste zaprosili za združitev večih uporabniških računov.
            Da lahko zagotovimo, da ste vi res vi se morate sedaj na vseh svojih mail računih (ki ste jih poslali za združitev)
            klikniti na link: """+ project_settings.BASE_URL +"""/merge_users_request/"""+str(rand)+" Za potrditev imate 6 ur časa. V kolikor niste zaprosili, prosimo, da ta email ignorirate."

            send_mail_to_user('ACM confirm', message, [email], False)

            em=email_merge(Child_mail=email,Master_mail=masterEmail,Potrjen=0,token=rand)
            em.save()

    return Response("success",status=201)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsSuperUser))
def send_test_mail(request):
    try: 
        testEmail = request.data["testEmail"]
    except:
        return Response("no_required_parameters",status=400)

    if not haveSMTPSettings():
        return JsonResponse({"response": 'brez_nastavitev'}, status=400)

    if testEmail == "":
        return JsonResponse({"response":"email_naslov_ni_vnesen"},status=400)

    send = send_mail_to_user('ACM test email', """Pozdravljeni. To je sistemski testni email,
        ki se uporablja pri pošiljanju v aplikaciji (ob registraciji, zamenjavi gesla, pozabljenem geslu itd.).
        V primeru, da ste to sporočilo dobili, pomeni, da pošiljanje deluje.""", [testEmail], False)
    if send == False:
        return JsonResponse({"response":"napaka_pri_posiljanju"},status=400)
    else:
        return Response()

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsSuperUser))
def can_decrypt_smtp_password(request):
    return JsonResponse({'have_smtp_settings':haveSMTPSettings(), 'have_fernet_key': haveFernetSecretKey(), 'have_correct_fernet_key': haveCorrectFernetKey(), 'can_decrypt_password': canDecryptPassword()},status=200)

@api_view(['POST'])
@permission_classes((IsAuthenticated,IsSuperUser))
def is_correct_smtp_password(request):
    try:
        password = request.data["smtpPassword"]
    except:
        return Response("no_required_parameters",status=400)

    if not haveSMTPSettings():
        return Response('no_smtp_settings', status=400)
    if not haveFernetSecretKey():
        return Response('dont_have_fernet_key_variable_in_configutation', status=400)
    if not haveCorrectFernetKey():
        return Response('incorrect_fernet_key_variable_value_change_it', status=400)
    if not canDecryptPassword():
        return Response('cant_decrypt_password_check_fernet_key', status=400)

    if getSMTPSettingsOriginalPassword() == password:
        return JsonResponse({"password_match": True})
    else:
        return JsonResponse({"password_match": False})