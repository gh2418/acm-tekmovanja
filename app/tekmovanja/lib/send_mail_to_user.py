from django.core.mail import get_connection, send_mail, send_mass_mail
from tekmovanja.models import *
from cryptography.fernet import Fernet, InvalidToken
from site_config.settings import *

def haveSMTPSettings():
    return get_smtp_settings() != None

def get_smtp_settings():
    return system_email_settings.load()

def getSMTPSettingsOriginalPassword():
    email_settings = get_smtp_settings()
    if not haveSMTPSettings():
        return None
    else:
        try:
            encryption_type = Fernet(SMTP_PASSWORD_SECRET_KEY)
        except:
            return None
        try:
            return encryption_type.decrypt(email_settings.email_host_password.encode()).decode()
        except InvalidToken:
            return None

def get_smtp_connection():
    if haveSMTPSettings():
        email_settings=get_smtp_settings()
        return get_connection(      
            host=email_settings.email_host, 
            port=email_settings.email_port, 
            username=email_settings.email_host_user, 
            password=getSMTPSettingsOriginalPassword(),
            use_tls = email_settings.email_use_tls)
    else:
        return None

def send_mail_to_user(subject, message, recipient_list, fail_silently):
    email_from = getFromEmail()
    connection = get_smtp_connection()

    if not haveSMTPSettings():
        return False

    try:
        send_mail(subject, message, email_from, recipient_list, fail_silently=False, connection = connection)
    except:
        try:
            connection.close()
        except:
            return False
        return False

    try:
        connection.close()
    except:
        return True
    return True

def send_mass_mail_to_users(messages):
    if not haveSMTPSettings():
        return False

    connection = get_smtp_connection()
    if haveSMTPSettings():
        send_mass_mail(messages, connection = connection)
        try:
            connection.close()
        except:
            return True
        return True
    else:
        return False

def haveFernetSecretKey():
    try:
        fernet_key = SMTP_PASSWORD_SECRET_KEY
    except:
        return False
    return True

def haveCorrectFernetKey():
    try:
        Fernet(SMTP_PASSWORD_SECRET_KEY)
    except:
        return False
    return True

def canDecryptPassword():
    can_decrypt_password = None
    if get_smtp_settings() != None:
        can_decrypt_password = getSMTPSettingsOriginalPassword() != None
    return can_decrypt_password

def getFromEmail():
    email_from = None
    if haveSMTPSettings():
        email_from = get_smtp_settings().email_from
    return email_from
