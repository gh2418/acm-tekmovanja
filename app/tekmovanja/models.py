from django.db import models
from django.contrib.auth.models import User
from cryptography.fernet import Fernet, InvalidToken
from site_config.settings import *
from django.core.exceptions import ValidationError

# User email as unique.
User._meta.get_field('email')._unique = True

class Verifikacija(models.Model):
    user = models.OneToOneField(User,unique=True, null=False, db_index=True,on_delete=models.CASCADE)
    token = models.TextField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

class Lastlogin(models.Model):
    user = models.OneToOneField(User,unique=True, null=False, db_index=True,on_delete=models.CASCADE)
    last_login = models.DateTimeField(auto_now_add=True)


class Pozadbljenogesloverifikacija(models.Model):
    user = models.OneToOneField(User,unique=True, null=False, db_index=True,on_delete=models.CASCADE)
    token = models.TextField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

class Sola(models.Model):
    ID_Sola = models.AutoField(db_column='ID_Sola',primary_key=True)
    Naziv = models.CharField(db_column='Naziv',max_length=105)
    Vrsta_Sola = models.CharField(db_column='Vrsta_Sola',max_length=5)
    created_by = models.ForeignKey(User, db_column='ID_Mentor',on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)

class Potrditve(models.Model):
    ID_Potrditve = models.AutoField(db_column='ID_Potrditve',primary_key=True)
    ID_Sola_potrditev = models.ForeignKey(Sola,on_delete=models.CASCADE,related_name='ID_Sola_potrditev')
    Zahteva= models.ForeignKey(User, on_delete=models.CASCADE,related_name='Zahteva')
    Potrditelj = models.ForeignKey(User, on_delete=models.CASCADE,related_name='Potrditelj')

    class Meta:
        db_table = 'Potrditve'

class email_merge(models.Model):
    ID_email_merge = models.AutoField(db_column='ID_email_merge',primary_key=True)
    Child_mail = models.CharField(db_column='Child_mail', max_length=50)
    Master_mail = models.CharField(db_column='Master_mail', max_length=50)
    Potrjen =  models.IntegerField(db_column='Potrjen')
    token = models.TextField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'email_merge'

class Uci(models.Model):
    ID_Mentor = models.ForeignKey(User,db_column='ID_Mentor',on_delete=models.CASCADE)
    ID_Sola = models.ForeignKey(Sola,db_column='ID_Sola',on_delete=models.PROTECT)
    Potrjen = models.IntegerField(db_column='Potrjen')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'Uci'
        unique_together = (('ID_Mentor', 'ID_Sola'),)

class tip_tekmovanje(models.Model):
    id = models.AutoField(db_column='id',primary_key=True)
    naziv = models.CharField(db_column='title',max_length=50)

class Tekmovanje(models.Model):
    ID_Tekmovanje = models.AutoField(db_column='ID_Tekmmovanje',primary_key=True)
    Ime_Tekmovanje = models.CharField(db_column='Ime_Tekmovanje',max_length=50)
    obdobje = models.IntegerField(db_column='obdobje')
    competition_date = models.DateField(db_column='competition_date')
    competition_date_from = models.DateField(db_column='competition_date_from')
    competition_date_to = models.DateField(db_column='competition_date_to')
    start_login_date = models.DateField(db_column='start_login_date')
    tip_tekmovanja = models.ForeignKey(tip_tekmovanje, db_column='tip_tekmovanja',on_delete=models.PROTECT)
    url_razpis = models.CharField(db_column='url_razpis',max_length=70)
    kontaktni_naslov = models.CharField(db_column='kontaktni_naslov',max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, db_column='ID_Mentor',on_delete=models.PROTECT)
    dodatni_podatki = models.CharField(db_column='dodatni_podatki',max_length=60)

    class Meta:
        db_table = 'Tekmovanje'

class mail_group(models.Model):
    idSkupine = models.AutoField(db_column='idSkupine',primary_key=True)
    name = models.CharField(db_column='name',max_length=80)
    created_by = models.IntegerField(db_column='created_by')
    glob = models.IntegerField(db_column='glob')
    fun_or_const = models.IntegerField(db_column='fun_or_const')

    class Meta:
        db_table = 'mail_group'

class Mail_settings(models.Model):
    ID_Setting = models.AutoField(db_column='ID_Setting',primary_key=True)
    streznik = models.CharField(db_column='Streznik',max_length=80)
    st_vrat = models.IntegerField(db_column='St_vrat')
    posta = models.CharField(db_column='Posta',max_length=80)
    mail_from = models.CharField(db_column='from',max_length=80, default='bober@acm.si')
    ID_Mentor = models.ForeignKey(User,db_column='ID_Mentor',on_delete=models.PROTECT)

    class Meta:
        db_table = 'Mail_settings'

class Sodeluje(models.Model):
    ID_Mentor = models.ForeignKey(User, db_column='ID_Mentor',on_delete=models.CASCADE)
    ID_Tekmovanje = models.ForeignKey(Tekmovanje, db_column='ID_Tekmovanje',on_delete=models.PROTECT)
    ID_Sola = models.ForeignKey(Sola, db_column='ID_Sola', on_delete=models.CASCADE)

    class Meta:
        db_table = 'Sodeluje'
        unique_together = (('ID_Mentor', 'ID_Tekmovanje','ID_Sola'),)

class Skritatekmovanja(models.Model):
    ID_Mentor = models.ForeignKey(User, db_column='ID_Mentor',on_delete=models.CASCADE)
    ID_Tekmovanje = models.ForeignKey(Tekmovanje, db_column='ID_Tekmovanje',on_delete=models.PROTECT)
    Hidden = models.IntegerField(db_column='Hidden')
    class Meta:
        db_table = 'Skritatekmovanja'
        unique_together = (('ID_Mentor', 'ID_Tekmovanje'),)

class mail_user_groups(models.Model):
    user_id = models.ForeignKey(User, db_column='user_id',on_delete=models.CASCADE)
    group_id = models.ForeignKey(mail_group, db_column='group_id',on_delete=models.CASCADE)
    class Meta:
        db_table = 'mail_user_groups'
        unique_together = (('user_id', 'group_id'),)

class user_email_change(models.Model):
    user_id = models.ForeignKey(User, db_column='user_id', on_delete=models.CASCADE)
    new_email = models.CharField(db_column='new_email',max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    token = models.TextField(max_length=500, blank=True) 
    class Meta:
        db_table = 'user_email_change'

class user_help(models.Model):
    title = models.CharField(db_column='title',max_length=40)
    description = models.CharField(db_column='description',max_length=500)

class Messages(models.Model):
    user_id = models.ForeignKey(User, db_column='user_id',on_delete=models.CASCADE)
    title = models.CharField(db_column='title',max_length=40)
    message = models.CharField(db_column='message',max_length=150)
    read = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

class SingletonModel(models.Model):
    class Meta:
        abstract = True
    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.pk == 1:
            pass
        else:
            super(SingletonModel, self).delete(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get(pk=1)
        except cls.DoesNotExist:
            return None

class system_email_settings(SingletonModel):
    system_email_settings_id = models.AutoField(db_column='system_email_settings_id', primary_key=True)
    email_use_tls = models.BooleanField()
    email_host = models.CharField(db_column='email_host', max_length=90)
    email_port = models.IntegerField(db_column='email_port')
    email_host_user = models.CharField(db_column='email_host_user', max_length=60)
    email_host_password = models.CharField(db_column='email_host_password', max_length=250)
    email_from = models.CharField(db_column='email_from', max_length=60)

    def my_property(self):
        return self.email_host + ' ' + self.email_port

    def save(self, *args, **kwargs):
        if self.pk != 1 and system_email_settings.objects.count() >= 1:
            raise ValidationError('There can be only one system_email_settings instance')
        try:
            encryption_type = Fernet(SMTP_PASSWORD_SECRET_KEY)
        except:
            raise ValidationError('No SMTP secret key')

        try:
            decrypted_message = encryption_type.decrypt(self.email_host_password.encode(), None).decode()
        except InvalidToken:
            self.email_host_password = encryption_type.encrypt(self.email_host_password.encode()).decode()

        super(system_email_settings, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Nastavitve SMTP strežnika"
