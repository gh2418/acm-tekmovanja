from django.contrib.auth.models import User, Group
from tekmovanja.models import *
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        model = User
        fields = ('id','url', 'username', 'email','is_staff','is_superuser','first_name','last_name','is_active')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class SolaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sola

        fields = '__all__'


class PotrditveSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Potrditve
        fields = '__all__'

class UciSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Uci
        fields = '__all__'

class TekmovanjeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tekmovanje
        fields = '__all__'

class Mail_groupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = mail_group
        fields = '__all__'

class Mail_settingsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mail_settings
        fields = '__all__'

class SodelujeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sodeluje
        fields = '__all__'

class Mail_user_groupsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = mail_user_groups
        fields = '__all__'

# Some where in the serializers.py file, add the following lines

class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)