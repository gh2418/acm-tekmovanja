var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        ime: "",
        priimek: "",
        email: "",
        up_ime: "",
        geslo: "",
        geslo_ponovno: "",
        adminStatus: "",
        adminAddIsSuccess: false,
        adminAddHasError: false,
      },
    methods: {
      add_admin() {
        var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
        }

        if(this.ime == "" || this.priimek == "" || this.up_ime == "" || this.email == "") {
          this.adminStatus="Vnesti morate vse podatke";
          this.adminAddIsSuccess = false;
          this.adminAddHasError = true;
          return;
        }
        if(this.geslo !== this.geslo_ponovno) {
          this.adminStatus="Gesli se ne ujemata"
          this.adminAddIsSuccess = false;
          this.adminAddHasError = true;
          return;
        }
        if(this.geslo.length < 5) {
          this.adminStatus="Geslo mora biti dolgo vsaj 5 znakov";
          this.adminAddIsSuccess = false;
          this.adminAddHasError = true;
          return;
        }
        let data = {
          'Ime': this.ime,
          'Priimek': this.priimek,
          'UpIme': this.up_ime,
          'email': this.email,
          'geslo': this.geslo,
        };
        if (confirm('Ste prepričani, da želite dodati novega admina?')) {
          this.adminStatus = "Pošiljanje ..."
          axios
          .post("/api/upravitelj_tekmovanj/add_admin_user/", data, {headers: headers})
          .then((response) => {
            if(response.status === 202) {
                this.adminStatus = "Administrator z up. imenom "+this.up_ime+" uspešno dodan.";
                this.adminAddIsSuccess = true;
                this.adminAddHasError = false;
            }
          })
          .catch(error => {
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 407) {
              this.adminStatus = "Uporabniško ime ne sme vsebovati znaka @";
              this.adminAddIsSuccess = false;
              this.adminAddHasError = true;
            }
            if(error.response.status === 409) {
                switch(error.response.data) {
                  case 'too_long' : this.adminStatus = "Uporabniško ime ali email naslov sta predolga"; break;
                  case 'data_missing': this.adminStatus = "Mankajoči obvezni podatki"; break;
                  case 'email_exists': this.adminStatus = "Email naslov že obstaja"; break;
                  case 'up_ime_exists': this.adminStatus = "Uporabniško ime že obstaja"; break;
                }
                this.adminAddIsSuccess = false;
                this.adminAddHasError = true;
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        }
      }
    }
  });