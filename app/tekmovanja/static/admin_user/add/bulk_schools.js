var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        textareaValue: "",
        status:[],
        options: [
            { value: 0, text: 'Osnovna šola' },
            { value: 1, text: 'Srednja šola' },
        ],
        selected: ""
    },
    methods: {
        bulk: function() {
                this.status = [];
                var headers = {
                    'X-CSRFToken': getCookie("csrftoken"),
                  }

                  if (this.selected === "") {
                      this.status.push("Izbrati morate tip šole");
                      return;
                  }

                  let schools = this.textareaValue.split("\n").filter(x => x !== "");
                  
                  if(schools.length == 0) {
                    this.status.push("Vnesti morate vsaj 1 šolo");
                    return;
                  }

                  var data = {
                    vrsta: this.selected,
                    schools: schools
                  };

                  this.status.push("Pošiljanje...");
                  axios
                  .post("/api/upravitelj_tekmovanj/bulk_school/", data, {headers:headers})
                  .then((response) => {
                    if(response) {
                        this.status.push(response.data + " šol uspešno vnešenih");
                    }
                  })
                  .catch(error => {
                    switch(error.response.data) {
                        case 'no_required_parameters' : this.status.push("Napaka pri procesiranju: manjkajoči parametri"); break;
                        case 'prazen_seznam': this.status.push("Napaka pri procesiranju: Prazen seznam šol"); break;
                        case 'podvojeni_podatki': this.status.push("Napaka pri procesiranju: Podvojeni podatki"); break;
                        case 'no_data' : this.status.push("Napaka pri procesiranju: prazen seznam uporabnikov (vnesite vsaj 1 uporabnika)"); break;
                        case 'invalid parameter': this.status.push("Napaka pri procesiranju: Napaka pri pocesiranju parametrov"); break;
                        default: this.status.push("Napaka pri procesiranju na zalednem delu")
                    }
                  });
            },
        }
    })