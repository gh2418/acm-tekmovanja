var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        //podatki iz forme
        status_tekmovanje: "",
        competitionName: "",
        datum_tekmovanja:"",
        datum_tekmovanja_od:"",
        datum_tekmovanja_do:"",
        datum_prijave:"",
        dodatni_podatki:"",
        competitionIsSuccess: false,
        competitionHasError: false,
        idTekmovanje: "",
        tekmovanje_dodano: false,
        types:"",
        emptyTypes: false,
        url_razpis:"",
        kontaktni_podatki:"",
        dodatni_podatki:"",
        selected_type:"",

        obdobje: '0',
        //podatki za modal
        nicerCompetitionDate: "",
        nicerloginDate: "",
        modal_url_razpis:"",
        modal_datum_prijave:"",
        modal_datum_tekmovanja:"",
        modal_kontaktni_podatki:"",
        modal_dodatni_podatki:"",
        tip_tekmovanja:"",
        create_user:"",
        modal_competition_name: "",
        modal_selected_obdobje: "",
        modal_tekmovanje_od: "",
        modal_tekmovanje_do:"",
        nicerCompetitionDate_from: "",
        nicerCompetitionDate_to: "",
      
        //tipi tekmovanj
        modal_selected_type:"",
        id_tip:"",
        
        currentCompetitionId: "",
        created_at: "",
        edit_status: "",
        delete_status: "",
        adminDeleteSuccess: false,
        adminDeleteHasError: false,
        adminEditSuccess: false,
        adminEditHasError: false,
        default_competition_name: "",

        //pove ali je modal visible ali ne
        competitionExists: true,
    },
    methods: {
        add_competition: function() {
            var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
            
            if (this.obdobje === "0") {
              if(this.datum_tekmovanja === "") {
                this.status_tekmovanje = "Vnesti morate vse zahtevane podatke";
                return;
              }
            } else {
              if(this.datum_tekmovanja_od === "" || this.datum_tekmovanja_do === "") {
                this.status_tekmovanje = "Vnesti morate vse zahtevane podatke";
                return;
              }
            }

            if(this.competitionName === "" || this.datum_prijave === "" || this.dodatni_podatki ==="" || this.url_razpis ==="" || this.kontaktni_podatki ==="" || this.dodatni_podatki ==="" || this.selected_type ===""  ) {
              this.status_tekmovanje = "Vnesti morate vse zahtevane podatke";
              return;
            }

            params = {
              Ime_Tekmovanje:this.competitionName,
              datum_tekmovanja:this.datum_tekmovanja,
              datum_prijave:this.datum_prijave,
              dodatni_podatki:this.dodatni_podatki,
              url_razpis:this.url_razpis,
              kontaktni_podatki:this.kontaktni_podatki,
              obdobje: this.obdobje,
              selected_type:this.selected_type,
            };

            if(this.obdobje === "0") {
              params.datum_tekmovanja = this.datum_tekmovanja;
            } else {
              params.datum_tekmovanja_od = this.datum_tekmovanja_od;
              params.datum_tekmovanja_do = this.datum_tekmovanja_do;
            }

            axios
            .post("/api/upravitelj_tekmovanj/admin_add_competition_call/", params, {headers:headers})
            .then((response) => {
                if(response.status == 201) {
                    this.status_tekmovanje = "Tekmovanje z imenom "+this.competitionName+" dodano";
                    this.idTekmovanje=response.data;
                    this.tekmovanje_dodano = true;
                    this.competitionIsSuccess = true;
                    this.competitionHasError = false;
                }
            })
            .catch(error => {
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 409) {
                this.status_tekmovanje = "Tekmovanje z isto vnešenim imenom in tipom že obstaja";
                this.competitionIsSuccess = false;
                this.competitionHasError = true;
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
        },
        competition_types() {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          axios
            .get("/api/upravitelj_tekmovanj/get_competition_types/", false,{headers: headers})
            .then((response) => {
              this.types=response.data;
              if(this.types.length === 0) {
                this.emptyTypes = true;
              }
            })
            .catch(error => {
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
        },

        //metode namenjene modulu
        getTekmovanje(competitionId) {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          this.edit_status="";
          this.delete_status = "";
          axios
          .get("/api/upravitelj_tekmovanj/get_competition/", {params: {competitionId:competitionId}}, {headers:headers})
          .then((response) => {
              if(response.status == 200) {
                  this.competitionExists = true;
                  this.modal_competition_name = response.data.Ime_Tekmovanje;
                  this.create_user = response.data.Ime_Uporabnika;
                  let date = response.data.created_at.split("T")[0].split("-");
                  this.created_at = date[2]+"."+date[1]+"."+date[0];
                  date = response.data.start_login_date.split("-");
                  this.nicerloginDate = date[2]+"."+date[1]+"."+date[0];
                  this.modal_datum_prijave = response.data.start_login_date;
                  this.modal_url_razpis = response.data.url_razpis;
                  this.modal_kontaktni_podatki = response.data.kontaktni_naslov;
                  this.modal_dodatni_podatki = response.data.dodatni_podatki;
                  this.tip_tekmovanja = response.data.tip;
                  this.id_tip = response.data.id_tip;
                  this.default_competition_name = this.modal_competition_name;
                  this.currentCompetitionId = competitionId;
                  this.modal_selected_type = this.id_tip;
                  this.modal_selected_obdobje = response.data.obdobje+"";
                  //izpis datumov
                  if(this.modal_selected_obdobje === "0") {
                    date = response.data.competition_date.split("-");
                    this.nicerCompetitionDate = date[2]+"."+date[1]+"."+date[0];  
                    this.modal_datum_tekmovanja = response.data.competition_date;
                  } 
                  if(this.modal_selected_obdobje === "1") {
                    date = response.data.competition_date_from.split("-");
                    this.nicerCompetitionDate_from = date[2]+"."+date[1]+"."+date[0];  
                    date = response.data.competition_date_to.split("-");
                    this.nicerCompetitionDate_to = date[2]+"."+date[1]+"."+date[0];    
                    this.modal_tekmovanje_od = response.data.competition_date_from;
                    this.modal_tekmovanje_do = response.data.competition_date_to;
                  }
              }
            })
          .catch(error => {
              if(error.response.status == 400) {
                this.competitionExists=false;
              }
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
        },
        edit_competition(competitionId) {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
    
          let params = {
            competitionId:competitionId,
            Naziv:this.modal_competition_name,
            datum_prijave:this.modal_datum_prijave,
            url_razpis:this.modal_url_razpis,
            kontaktni_naslov:this.modal_kontaktni_podatki,
            dodatni_podatki:this.modal_dodatni_podatki,
            selected_type:this.modal_selected_type,
            obdobje: this.modal_selected_obdobje
          };
          if(this.modal_selected_obdobje === "0") {
            params.datum_tekmovanja = this.modal_datum_tekmovanja;
          } 
          if(this.modal_selected_obdobje === "1") {
            params.datum_tekmovanja_od = this.modal_tekmovanje_od;
            params.datum_tekmovanja_do = this.modal_tekmovanje_do;
          }
          axios
          .post("/api/upravitelj_tekmovanj/change_competition/", params, {headers:headers})
          .then((response) => {
              if(response.status == 202) {
                  this.edit_status = "Tekmovanje uspešno spremenjeno";
                  this.adminEditSuccess = true;
                  this.adminEditHasError = false;
              }
          })
          .catch(error => {
            if(error.response.status == 400 && error.response.data == "no_changes") {
              this.edit_status="Spremenite vsaj 1 podatek";
              this.adminEditSuccess = false;
              this.adminEditHasError = true;
            }
            if(error.response.status == 400 && error.response.data == "competition not exists") {
              this.edit_status="Tekmovanje ne obstaja";
              this.adminEditSuccess = false;
              this.adminEditHasError = true;
            }
            if(error.response.status == 400 && error.response.data == "data missing") {
              this.edit_status="Vsi podatki so za vnos obvezni";
              this.adminEditSuccess = false;
              this.adminEditHasError = true;
            }
            if(error.response.status == 400 && error.response.data == "data_missing") {
              this.edit_status="Manjkajoči podatki";
              this.adminEditSuccess = false;
              this.adminEditHasError = true;
            }
            if(error.response.status == 400 && error.response.data == "tekmovanje s tem imenom in tipom že obstaja") {
              this.edit_status="Tekmovanje s tem imenom in tipom že obstaja.";
              this.adminEditSuccess = false;
              this.adminEditHasError = true;
            }
            if(error.response.status == 409) {
              this.adminEditSuccess = false;
              this.adminEditHasError = true;
              this.edit_status="Napaka pri procesiranju parametrov.";
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        },
    
        delete_competition(competitionId) {
          var result = confirm("Ste prepričani, da želite izbrisati tekmovanje?");
              if (result) {
              var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
              axios
              .delete("/api/upravitelj_tekmovanj/delete_competition/?competitionId="+competitionId, {headers:headers})
              .then((response) => {
                  if(response.status == 202) {
                     window.location.reload();
                  }
              })
              .catch(error => {
              if(error.response.status == 404) {
                  alert("Tekmovanje ne obstaja");
              }
              if(error.response.status == 409) {
                switch(error.response.data) {
                  case 'competiton_id_not_set': this.delete_status="Competition id not set"; break;
                  case 'users_participate': this.delete_status="Ne morete izbrisati, ker mentorji sodelujejo na tekmovanju"; break;
                }
                this.adminDeleteSuccess = false;
                this.adminDeleteHasError = true;
              }
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                  console.log("Something went wrong");
              }
              });
          }
        },
        
    }
  });
  app.competition_types();