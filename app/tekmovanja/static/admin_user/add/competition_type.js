var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        status_tekmovanje: "",
        competition_type: "",
        competitionIsSuccess: false,
        competitionHasError: false,
    },
    methods: {
        add_competition_type: function() {
            var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
            if(this.competition_type === "") {
              this.status_tekmovanje = "Vnesti morate vse zahtevane podatke";
              return;
            }
            params = {
                competition_type:this.competition_type,
            };
            axios
            .post("/api/upravitelj_tekmovanj/add_competition_type/", params, {headers:headers})
            .then((response) => {
                if(response.status == 201) {
                    this.status_tekmovanje = "Vrsta tekmovanja z imenom "+this.competition_type+" dodano";
                    this.competitionIsSuccess = true;
                    this.competitionHasError = false;
                }
            })
            .catch(error => {
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 400 && error.response.data == "tip tekmovanja že obstaja") {
                this.status_tekmovanje = "Vrsta tekmovanja z imenom "+this.competition_type+" že obstaja";
                this.competitionIsSuccess = false;
                this.competitionHasError = true;
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
        },        
    }
  });