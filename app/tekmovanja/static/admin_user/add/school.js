var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        //modal data
        currentSchoolId: "",
        currentUser:"",
        modalDate: "",
        default_school_name: "",
        edit_status: "",
        delete_status: "",
        adminEditSuccess: false,
        adminEditHasError: false,
        adminDeleteSuccess: false,
        adminDeleteHasError: false,
        schoolAdded: false,
        addedSchoolId: "",
        schoolExists: "",
        schoolName: "",
        status_sola: "",
        schoolIsSuccess: false,
        schoolHasError: false,
        selected: null,
        options: [
          { value: 0, text: 'Osnovna šola' },
          { value: 1, text: 'Srednja šola' },
        ]
      },
    methods: {
        add_school: function() {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          if(this.schoolName === "") {
            this.status_sola = "Vnesti morate naziv šole";
            this.schoolIsSuccess = false;
            this.schoolHasError = true;
            return;
          }
          if(this.selected === null) {
            this.status_sola = "Izbrati morate vrsto šole";
            this.schoolIsSuccess = false;
            this.schoolHasError = true;
            return;
          }

          axios
          .post("/api/upravitelj_tekmovanj/admin_add_school_call/", {Naziv:this.schoolName, vrsta:this.selected}, {headers:headers})
          .then((response) => {
              if(response.status == 201) {
                this.status_sola = "Šola z imenom "+this.schoolName+" uspešno dodana";
                this.addedSchoolId = response.data;
                this.schoolAdded = true;
                this.schoolIsSuccess = true;
                this.schoolHasError = false;
              }
          })
          .catch(error => {
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 409) {
              this.status_sola="Šola z imenom "+this.schoolName+" že obstaja.";
              this.schoolIsSuccess = false;
              this.schoolHasError = true;
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
      },

      //modal functions 
      get_school(schoolId) {
        this.edit_status="";
        this.delete_status="";
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/upravitelj_tekmovanj/get_school/", {params: {schoolId:schoolId}}, {headers:headers})
        .then((response) => {
            if(response.status == 200) {
                this.schoolExists = true;
                this.schoolName = response.data.Naziv;
                this.currentSchoolId = schoolId;
                this.currentUser = response.data.Ime_Uporabnika;
                let date = response.data.created_at.split("T")[0].split("-");
                this.modalDate = date[2]+"."+date[1]+"."+date[0];
                this.default_school_name = this.schoolName;
            }
          })
        .catch(error => {
            if(error.response.status == 400) {  
              this.schoolExists = false;
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        },
        edit_school(schoolId) {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          if(this.schoolName == this.default_school_name) {
            this.edit_status="Šoli morate spremeniti ime";
            this.adminEditSuccess=false;
            this.adminEditHasError=true;
            return 1;
          }
          axios
          .post("/api/upravitelj_tekmovanj/change_school_name/", {schoolId:schoolId,Naziv:this.schoolName}, {headers:headers})
          .then((response) => {
              if(response.status == 201) {
                  this.edit_status = "Naziv šole uspešno spremenjen";
                  this.adminEditSuccess=true;
                  this.adminEditHasError=false;
              }
          })
          .catch(error => {
            if(error.response.status == 400) {
              this.edit_status="Šola, ki jo želite spremeniti ne obstaja";
              this.adminEditSuccess=false;
              this.adminEditHasError=true;
            }
            if(error.response.status == 409) {
              alert("Napaka pri procesiranju parametrov.");
            }
            if(error.response.status == 403) {     
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 405) {
              this.edit_status="Šola s tem imenom že obstaja";
              this.adminEditSuccess=false;
              this.adminEditHasError=true;
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        },

      delete_school(event, schoolId) {
        var result = confirm("Ste prepričani, da želite izbrisati šolo?");
            if (result) {
            var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
            axios
            .delete("/api/upravitelj_tekmovanj/delete_school/?schoolId="+ schoolId, {headers:headers})
            .then((response) => {
                if(response.status == 202) {
                    let button = document.getElementById(event.target.id);
                    button.style="background-color:green";
                    button.innerHTML="Šola izbrisana";
                    button.disabled=true; 
                    window.location.reload();
                }
            })
            .catch(error => {
            if(error.response.status == 404) {
                alert("Šola ne obstaja");
            }
            if(error.response.status == 409) {
              switch(error.response.data) {
                case 'no_parameters': this.delete_status="Za šolo ni parametrov"; break;
                case 'users_exists': this.delete_status="Na šoli že učijo mentorji"; break;
              }
              this.adminDeleteSuccess=false;
              this.adminDeleteHasError=true;
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
                console.log("Something went wrong");
            }
            });
        }
      },
    }
  });