Vue.use(VueTables.ClientTable);
var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      tableData: [
      ],
      options: {
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
      columns: ['title','izbris'],
    },
    methods: {
      get_competition_types: function() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
       axios
        .get("/api/upravitelj_tekmovanj/get_competition_types/", false,{headers: headers})
        .then((response) => {
          this.tableData=response.data;
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Napaka pri pridobivanju vrst tekmovanj.");
          }
        });
      },
      delete_competition_type(id) {
        var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
        params = {
            id:id
        }
        var result = confirm("Ste prepričani, da želite izbrisati vrsto tekmovanj?");
        if (result) {
            axios
            .delete("/api/upravitelj_tekmovanj/delete_competition_type/?competitionTypeId="+id, {headers: headers})
            .then((response) => {
              this.tableData = this.tableData.filter(x => x.id !== id);
            })
            .catch(error => {
                if(error.response.status == 400 && error.response.data == "uporabljeno na tekmovanju") {
                  alert("Tip tekmovanja uporabljen na vsaj 1 tekmovanju, zato brisanje ni mogoče.");
                }
                if(error.response.status == 400 && error.response.data != "uporabljeno na tekmovanju") {
                  alert("Napaka parametri.");
                }
                if(error.response.status == 403) {
                  alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
                }
                if(error.response.status == 500) {
                    alert("Napaka pri brisanju vrste tekmovanj.");
                }
            });
        }
      }
    },
  });
app.get_competition_types();
