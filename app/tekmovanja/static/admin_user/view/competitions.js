Vue.use(VueTables.ClientTable);

var app = new Vue({
  delimiters: ['[[', ']]'],
  el: "#container",
  data: {
      //modal variables
      //boljsi izpis datumov
      nicerCompetitionDate: "",
      nicerloginDate: "",
      loadingTekmovanje: false,
      datum_tekmovanja:"",
      datum_prijave:"",
      url_razpis:"",
      kontaktni_naslov:"",
      dodatni_podatki:"",
      tip_tekmovanja:"",
      types:"",
      selected_type:"",
      id_tip:"",
      currentCompetitionName: "",
      currentCompetitionId: "",
      modalDate: "",
      edit_status: "",
      delete_status: "",
      adminDeleteSuccess: false,
      adminDeleteHasError: false,
      adminEditSuccess: false,
      adminEditHasError: false,
      mentorjiTekmovanjeError: false,
      getCompetitionError: false,
      default_competition_name: "",
      currentUser:"",
      
      //mentorji na tekmovanju
      loadingMentors: false,
      mentorjiTekmovanje: [],
      //obdobje
      obdobje: '0',
      modal_datum_tekmovanja: "",
      modal_selected_obdobje: "",
      modal_tekmovanje_od: "",
      modal_tekmovanje_do:"",
      nicerCompetitionDate_from: "",
      nicerCompetitionDate_to: "",
    
      //pove ali je modal visible ali ne
      competitionExists: true,
      columns: ['Ime_Tekmovanje','tip_tekmovanja','competition_date','start_login_date'],
      tableData: [
      ],
      options: {
        headings: {
          school_competition_date: 'Datum šolskega',
          competition_date: 'Datum tekmovanja',
          start_login_date: 'Začetek prijave',
        },
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
      sortable: ['Ime_Tekmovanje'],
      filterable: ['Ime_Tekmovanje']
  },
  methods: {
    getTekmovanja: function() {
      var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
      axios
      .get("/api/upravitelj_tekmovanj/get_competitions/", false,{headers: headers})
      .then((response) => {
        if(response.status == 200) {
          var data = response.data.map(function(el) {
            let date = "";
            if(el.obdobje === 0) {
              date = el.competition_date.split("-");
              el.competition_date = date[2]+"."+date[1]+"."+date[0];
              date = el.start_login_date.split("-");
              el.start_login_date = date[2]+"."+date[1]+"."+date[0];
              return el;
            }
            
            if(el.obdobje === 1) {
              date = el.competition_date_from.split("-");
              el.competition_date_from = date[2]+"."+date[1]+"."+date[0];
              date = el.competition_date_to.split("-");
              el.competition_date_to = date[2]+"."+date[1]+"."+date[0];
              date = el.start_login_date.split("-");
              el.start_login_date = date[2]+"."+date[1]+"."+date[0];
              return el;
            }

            return el;
          });
          this.tableData = data;
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    getTekmovanje(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }

      this.resetModalData(competitionId);

      this.getCompetitionError = false;
      this.loadingTekmovanje = true;
      axios
      .get("/api/upravitelj_tekmovanj/get_competition/", {params: {competitionId:competitionId}}, {headers:headers})
      .then((response) => {
          this.loadingTekmovanje = false;
          if(response.status == 200) {
              this.competitionExists = true; 
              this.currentCompetitionName = response.data.Ime_Tekmovanje;
              this.currentUser = response.data.Ime_Uporabnika;
              let date = response.data.created_at.split("T")[0].split("-");
              this.modalDate = date[2]+"."+date[1]+"."+date[0];
              date = response.data.start_login_date.split("-");
              this.nicerloginDate = date[2]+"."+date[1]+"."+date[0];
              this.datum_tekmovanja = response.data.competition_date;
              this.datum_prijave = response.data.start_login_date;
              this.url_razpis = response.data.url_razpis;
              this.kontaktni_naslov = response.data.kontaktni_naslov;
              this.dodatni_podatki = response.data.dodatni_podatki;
              this.tip_tekmovanja = response.data.tip;
              this.id_tip = response.data.id_tip;
              this.default_competition_name = this.currentCompetitionName;
              this.currentCompetitionId = competitionId;
              this.selected_type = this.id_tip;
              this.modal_selected_obdobje = response.data.obdobje+"";
              //izpis datumov
              if(this.modal_selected_obdobje === "0") {
                date = response.data.competition_date.split("-");
                this.nicerCompetitionDate = date[2]+"."+date[1]+"."+date[0];  
                this.modal_datum_tekmovanja = response.data.competition_date;
              } 
              if(this.modal_selected_obdobje === "1") {
                date = response.data.competition_date_from.split("-");
                this.nicerCompetitionDate_from = date[2]+"."+date[1]+"."+date[0];  
                date = response.data.competition_date_to.split("-");
                this.nicerCompetitionDate_to = date[2]+"."+date[1]+"."+date[0];    
                this.modal_tekmovanje_od = response.data.competition_date_from;
                this.modal_tekmovanje_do = response.data.competition_date_to;
              }
          }
        })
      .catch(error => {
          this.loadingTekmovanje = false;
          if(error.response.status == 400) {
            this.competitionExists=false;
          }
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 409) {
            this.getCompetitionError = true;
            alert("Pri pridobivanju podatkov o tekmovanju je šlo nekaj narobe (parametri), v pojavnem oknu ne bo prikazanih informacij o tekmovanju (to ne velja za mentorje na tekmovanju).");
          }
          if(error.response.status == 500) {
            this.getCompetitionError = true;
            alert("Pri pridobivanju podatkov o tekmovanju je šlo nekaj narobe, v pojavnem oknu ne bo prikazanih informacij o tekmovanju (to ne velja za mentorje na tekmovanju).");
          }
        });
    },

    getSodelujoce(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }

      this.loadingMentors = true;
      this.mentorjiTekmovanje = [];
      this.mentorjiTekmovanjeError = false;
      axios
      .get("/api/upravitelj_tekmovanj/get_menthors_by_competition/", {params: {competitionId:competitionId}}, {headers:headers})
      .then((response) => {
        this.loadingMentors = false;
        this.mentorjiTekmovanje = JSON.parse(response.data);
      })
      .catch(error => {
          this.loadingMentors = false;
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 409) {
            this.mentorjiTekmovanjeError = true;
            alert("Pri pridobivanju podatkov o mentorjih na tekmovanju je šlo nekaj narobe (parametri), prikazanih ne bo nobenih mentorjev.");
          }
          if(error.response.status == 500) {
            this.mentorjiTekmovanjeError = true;
            alert("Pri pridobivanju podatkov o mentorjih na tekmovanju je šlo nekaj narobe, prikazanih ne bo nobenih mentorjev.");
          }
        });
    },

    edit_competition(competitionId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }

      let params = {
        competitionId:competitionId,
        Naziv:this.currentCompetitionName,
        datum_prijave:this.datum_prijave,
        datum_tekmovanja:this.datum_tekmovanja,
        url_razpis:this.url_razpis,
        kontaktni_naslov:this.kontaktni_naslov,
        dodatni_podatki:this.dodatni_podatki,
        selected_type:this.selected_type,
        obdobje: this.modal_selected_obdobje
      };
      if(this.modal_selected_obdobje === "0") {
        params.datum_tekmovanja = this.modal_datum_tekmovanja;
      } 
      if(this.modal_selected_obdobje === "1") {
        params.datum_tekmovanja_od = this.modal_tekmovanje_od;
        params.datum_tekmovanja_do = this.modal_tekmovanje_do;
      }
      axios
      .post("/api/upravitelj_tekmovanj/change_competition/", params, {headers:headers})
      .then((response) => {
          if(response.status == 202) {
              this.edit_status = "Tekmovanje uspešno spremenjeno. Za spremembe v tabeli ponovno naložite stran.";
              this.adminEditSuccess = true;
              this.adminEditHasError = false;
          }
      })
      .catch(error => {
        if(error.response.status == 400 && error.response.data == "no_changes") {
          this.edit_status="Spremenite vsaj 1 podatek";
          this.adminEditSuccess = false;
          this.adminEditHasError = true;
        }
        if(error.response.status == 400 && error.response.data == "competition not exists") {
          this.edit_status="Tekmovanje ne obstaja";
          this.adminEditSuccess = false;
          this.adminEditHasError = true;
        }
        if(error.response.status == 400 && error.response.data == "data missing") {
          this.edit_status="Vsi podatki so za vnos obvezni";
          this.adminEditSuccess = false;
          this.adminEditHasError = true;
        }
        if(error.response.status == 400 && error.response.data == "data_missing") {
          this.edit_status="Manjkajoči podatki";
          this.adminEditSuccess = false;
          this.adminEditHasError = true;
        }
        if(error.response.status == 400 && error.response.data == "tekmovanje s tem imenom in tipom že obstaja") {
          this.edit_status="Tekmovanje s tem imenom in tipom že obstaja.";
          this.adminEditSuccess = false;
          this.adminEditHasError = true;
        }
        if(error.response.status == 409) {
          this.adminEditSuccess = false;
          this.adminEditHasError = true;
          this.edit_status="Napaka pri procesiranju parametrov.";
        }
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },
        competition_types() {
          var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
          axios
            .get("/api/upravitelj_tekmovanj/get_competition_types/", false,{headers: headers})
            .then((response) => {
              this.types=response.data;
            })
            .catch(error => {
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
        },

    delete_competition(event, competitionId) {
      var result = confirm("Ste prepričani, da želite izbrisati tekmovanje?");
          if (result) {
          var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
          axios
          .delete("/api/upravitelj_tekmovanj/delete_competition/?competitionId="+competitionId, {headers:headers})
          .then((response) => {
              if(response.status == 202) {
                  let button = document.getElementById(event.target.id);
                  button.style="background-color:green";
                  button.innerHTML="Tekmovanje izbrisano";
                  button.disabled=true;
                  this.tableData = this.tableData.filter(x => x.ID_Tekmovanje !== competitionId);
                  this.adminDeleteSuccess = true;
                  this.adminDeleteHasError = false;
              }
          })
          .catch(error => {
          if(error.response.status == 404) {
              alert("Tekmovanje ne obstaja");
          }
          if(error.response.status == 409) {
            switch(error.response.data) {
              case 'competiton_id_not_set': this.delete_status="Competition id not set"; break;
              case 'users_participate': this.delete_status="Na tem tekmovanju sodelujejo mentorji, zato ga ne morete izbrisati. "; break;
            }
            this.adminDeleteSuccess = false;
            this.adminDeleteHasError = true;
          }
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
              console.log("Something went wrong");
          }
          });
      }
    },

    // Reset data from modal (before request)
    resetModalData(competitionId) {
      this.edit_status="";
      this.delete_status = "";
      this.competitionExists = true;
      this.currentCompetitionName = "";
      this.currentUser = "";
      this.modalDate = "";
      this.nicerloginDate = "";
      this.datum_tekmovanja = "";
      this.datum_prijave = "";
      this.url_razpis = "";
      this.kontaktni_naslov = "";
      this.dodatni_podatki = "";
      this.tip_tekmovanja = "";
      this.id_tip = "";
      this.default_competition_name = "";
      this.currentCompetitionId = competitionId;
      this.selected_type = "";
      this.modal_selected_obdobje = "";
      this.nicerCompetitionDate = "";
      this.modal_datum_tekmovanja = "";
      this.nicerCompetitionDate_from = "";
      this.nicerCompetitionDate_to = "";
      this.modal_tekmovanje_od = "";
      this.modal_tekmovanje_do = "";
    },

    searchModalMentorsTable() {
      // Declare variables
      let input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("modalMentorSearch");
      filter = input.value.toUpperCase();
      table = document.getElementById("modalMentorsTable");
      tr = table.getElementsByTagName("tr");
    
      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    },

    downloadCsv(text, fileType, fileName) {
      var blob = new Blob([text], { type: fileType })
      var a = document.createElement('a');
      a.download = fileName;
      a.href = URL.createObjectURL(blob);
      a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
      a.style.display = "none";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
    },
    toCsv: function() {
      let newTable=this.tableData.filter(el => el !== null);
      this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
    },
    mentorsToCsv: function() {
      input = document.getElementById("modalMentorSearch").value;
      let newTable=this.mentorjiTekmovanje.filter(el => el !== null && el.email.toUpperCase().includes(input.toUpperCase()));
      this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
    },
  },
});
app.getTekmovanja();
app.competition_types();
