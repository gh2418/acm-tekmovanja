var emailRegExp = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      ime: "",
      priimek: "",
      up_ime: "",
      email: "",
      status: "",
      geslo_status: "",
      geslo_status2: "",
      geslo1: "",
      geslo2: "",
      geslo3: "",
      user: "",
      izbris_status: "",
      emailSettings: []
    },
    methods: {
      onInit() {
        if(localStorage.getItem('user')) {
          this.user = JSON.parse(localStorage.getItem('user'));
        } else {
          this.get_user_info();
        }
        this.ime = this.user.first_name;
        this.priimek = this.user.last_name;
        this.up_ime = this.user.username;
        this.email = this.user.email;

        this.get_mail_settings();
      },

      get_user_info: function() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/user/get_user_info/", false, {headers:headers})
        .then((response) => {
          if(response) {
              localStorage.setItem('user', JSON.stringify(response.data.user));
              this.user = JSON.parse(localStorage.getItem('user'));
            }
        })
        .catch(error => {
          alert("Nekaj je šlo narobe prosimo prijavite se ponovno");
        });
      },

      get_mail_settings() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
          .get("/api/upravitelj_tekmovanj/get_user_settings/id?idU="+this.user.id, false, {headers:headers})
          .then((response) => {
            this.emailSettings = response.data;
            let settingKey = 1;
            this.emailSettings.forEach(function(e) {
              e.settingKeyId=settingKey; 
              settingKey= settingKey+1;
            });
          })
          .catch(error => {
            alert("Nekaj je šlo narobe prosimo prijavite se ponovno");
          });
      },

      add_email_setting() {
        let lastItem = this.emailSettings[this.emailSettings.length -1];

        if(lastItem === undefined) {
          lastKeyId = 0;
        } else {
          lastKeyId = lastItem.settingKeyId;
        }
        
        this.emailSettings.push({ID_Setting: -1,streznik: "", st_vrat: "", posta: "", mail_from: '', settingKeyId: lastKeyId + 1});
      },

      delete_email_setting(settingKeyId) {
        let popup = confirm("Ste prepričani, da želite izbrisati nastavitev?");
        if(popup) {
          this.emailSettings = this.emailSettings.filter(e => e.settingKeyId !== settingKeyId);
        }
      },
      
      change_data() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        let data = {
          'Ime': this.ime,
          'Priimek': this.priimek,
          'UpIme': this.up_ime,
          //'email': this.email,
        };

        if(this.ime == this.user.first_name && this.priimek == this.user.last_name && this.up_ime == this.user.username && this.email == this.user.email) {
          this.status="Spremenili niste nobenega podatka.";
          return;
        }
        if (confirm('Ste prepričani, da želite spremeniti podatke?')) {
          this.status = "Pošiljanje ..."
          axios
          .post("/api/upravitelj_tekmovanj/admin_change_profile/", data, {headers: headers})
          .then((response) => {
            if(response.status === 201) { 
              this.status = "Podatki so uspešno spremenjeni, prosimo prijavite se ponovno.";
            }
          })
          .catch(error => {
            if(error.response.status == 400) {
              switch(error.response.data) {
                case 'up_ime_vsebuje_presledke': this.posiljanje_status = ""; this.status = "Uporbniško ime ne sme vsebovati presledkov pred ali za uporabniškim imenom."; break;
             }
            }
            if(error.response.status == 407) {
              this.status = "Uporabniško ime ne sme vsebovati znaka @";
            }
            if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
            if(error.response.status === 409) {
                switch(error.response.data) {
                    case 'mankajoci_podatki': this.status = "Manjkajoči podatki"; break;
                    case 'predolgo_up_ime_ali_email': this.status = "Uporabniško ime ali email sta predolga"; break;
                    case 'up_ime_zasedeno': this.status = "Uporabniško ime zasedeno"; break;
                    case 'email_zaseden': this.status = "Email naslov je že zaseden"; break;
                }
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
          });
        }
      },

      change_password() {
        if (this.geslo1 !== this.geslo2) {
          this.geslo_status = "Gesli se ne ujemata";
          return;
        }
        if (this.geslo1.length < 5) {
          this.geslo_status = "Geslo mora biti dolgo vsaj 5 znakov";
          return;
        }
        
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        let data = {
          'geslo': this.geslo1,
        };

        if (confirm('Ste prepričani, da želite spremeniti geslo?')) {
          this.geslo_status = "Pošiljanje ..."
          axios
          .post("/api/upravitelj_tekmovanj/admin_change_password/", data, {headers: headers})
          .then((response) => {
            if(response.status === 201) {      
              this.geslo_status = "Uspešna sprememba gesla. Ob ponovni prijavi boste morali vnesti novo geslo.";
            }
          })
          .catch(error => {
            if(error.response.status === 409) {
              this.geslo_status = "Napaka.";
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        }
      },

      delete_user() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        if (confirm('Ste prepričani, da se želite izbrisati?')) {
          axios
          .delete("/api/upravitelj_tekmovanj/delete_admin_user/",{headers: headers})
          .then((response) => {
            if(response.data === "zadnji_admin") {
              this.izbris_status = "Ker ste zadnji upravitelj tekmovanj v sistemu, se iz sistema ne morete izbrisati";
            } else {
              window.location.replace("/login");
            }  
          })
          .catch(error => {
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
              console.log("Something went wrong");
            }
          });
        }
      },

      saveSettings: function(){
        // Check input data
        let err=false;
        let errType = "";
        errItems = [];
        this.emailSettings.forEach(function(e) {
          if(e.streznik == "") {
            err = true;
            errType = "streznik";
            errItems = errItems.filter(el => el.settingKeyId !== e.settingKeyId);
            errItems.push(e);
          }

          if(e.st_vrat == "" && e.st_vrat !== 0) {
            err = true;
            errType = "st_vrat";
            errItems = errItems.filter(el => el.settingKeyId !== e.settingKeyId);
            errItems.push(e);
          }

          if(e.posta == "") {
            err = true;
            errType = "posta";
            errItems = errItems.filter(el => el.settingKeyId !== e.settingKeyId);
            errItems.push(e);  
          }

          if(e.mail_from == "") {
            err = true;
            errType = "mail_from";
            errItems = errItems.filter(el => el.settingKeyId !== e.settingKeyId);
            errItems.push(e);
          }

          if(emailRegExp.test(e.mail_from) == false) {
            err = true;
            errType = "mail_from_wrong_email_address";
            errItems = errItems.filter(el => el.settingKeyId !== e.settingKeyId);
            errItems.push(e);
          }
        }); 

        if(err) {
          switch(errType) {
            case "streznik": this.geslo_status2 = "Pri eni ali več nastavitvah niste vnesli ime strežnika, ki je za vnos obvezen."; break;
            case "st_vrat": this.geslo_status2 = "Pri eni ali več nastavitvah niste vnesli številko vrat, ki je za vnos obvezna."; break;
            case "posta": this.geslo_status2 = "Pri eni ali več nastavitvah niste vnesli SMTP uporabniškega imena, ki je za vnos obvezen."; break;
            case "mail_from": this.geslo_status2 = "Pri eni ali več nastavitvah niste vnesli pošiljatelja (from), ki je za vnos obvezen."; break;
            case "mail_from_wrong_email_address": this.geslo_status2 = "Pri eni ali več nastavitvah ste vnesli napačno obliko elektronskega naslova (from), ki je za vnos obvezen."; break;
            default: this.geslo_status2 = "Prišlo je do napake, preglejte vnose."; break;
          } 
          let errItemMsg=" Številke nastavitev, ki imajo napačne vnose: ";
          errItems.forEach(function(e) {
            errItemMsg+=e.settingKeyId+", ";
          });
                  
          errItemMsg = errItemMsg.slice(0,-2);
          this.geslo_status2+=errItemMsg;

          document.getElementsByClassName("text-info")[2].style.setProperty('color', 'red', 'important');
          document.getElementsByClassName("text-info")[2].style.textAlign = "center";
          return;
        } else {
          this.geslo_status2 = "";
        }

        // Call API FOR SAVING DATA
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        // pridobi admina
        if(localStorage.getItem('user')) {
          this.user = JSON.parse(localStorage.getItem('user'));
        } else {
          this.get_user_info();
        }
        
        admin_id = this.user.id;
        axios
          .post("/api/upravitelj_tekmovanj/admin_save_settings/", {emailSettings: this.emailSettings}, {headers: headers})
          .then((response) => {
            if(response.status === 201) {
              this.geslo_status2 = "Uspešno ste shranili nastavitve";
                document.getElementsByClassName("text-info")[2].style.setProperty('color', 'green', 'important');
                document.getElementsByClassName("text-info")[2].style.textAlign = "center";
              }
            })
          .catch(error => {
            if(error.response.status === 409) {
              this.geslo_status = "Napaka.";
            }
            if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
          }
    }
  });

  app.onInit();
