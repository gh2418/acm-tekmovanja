Vue.use(VueTables.ClientTable);
var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      loadingSchools: false,
      selectedSchoolId:"",
      unconfirmed_on_school:[],
      columns: ['Naziv'],
      tableData: [
      ],
      options: {
        headings: {
          Naziv: 'Naziv šole'
        },
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
    },
    methods: {
      userConfirm(event,userId) {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }

        axios
        .post("/api/shared/confirm_user/", {userId:userId,schoolId:this.selectedSchoolId},{headers: headers})
        .then((response) => {
          let button = document.getElementById(event.target.id);
          button.style="display:none";
          this.$bvToast.toast("Uporabnika ste uspešno potrdili", {
            title: "Uspešna potrditev",
            autoHideDelay: 3000,
            appendToast: true,
            variant: 'success',
            solid: true
          });
        })
        .catch(error => {
          if(error.response.status == 400) {
            alert(error.response.data);
          }
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      },

      getSchools: function() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        axios
        .get("/api/upravitelj_tekmovanj/get_first_uncorfirmed_menthor/",false,{ headers: headers } )
        .then((response) => {
          if(response.status == 200) {
            this.tableData = JSON.parse(response.data);
          }
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Napaka pri pridobivanju podatkov.");
          }
        });
      },
      getSchoolUnconfirmedMenthors(schoolId) {
        this.selectedSchoolId = schoolId;
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }

        this.loadingSchools = true;
        this.unconfirmed_on_school = [];
        axios
        .get("/api/upravitelj_tekmovanj/get_uncorfirmed_menthors_by_school/",{params: { schoolId:this.selectedSchoolId}},{headers:headers})
        .then((response) => {
          this.loadingSchools = false;
          if(response.status == 200) {
            this.unconfirmed_on_school = JSON.parse(response.data);
          }
        })
        .catch(error => {
          this.loadingSchools = false;
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Pri pridobivanju podatkov o šolah je šlo nekaj narobe");
          }
        });
      },

    downloadCsv(text, fileType, fileName) {
      var blob = new Blob([text], { type: fileType })
      var a = document.createElement('a');
      a.download = fileName;
      a.href = URL.createObjectURL(blob);
      a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
      a.style.display = "none";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
    },
    toCsv: function() {
      let newTable=this.tableData.filter(el => el !== null);
      this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
    },
      handler(userId) {
        this.getUser(userId);
        this.getUserCompetitions(userId);
        this.getUserSchoolsSubscriptions(userId);
      }
    },
  });
app.getSchools();
