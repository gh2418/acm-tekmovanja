Vue.use(VueTables.ClientTable);

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: "#container",
    data: {
        help: "",
        currentHelp: {id:"",title:"",description:""},
        tableData: [
        ],
        options: {
            texts: {
            filterPlaceholder:"Iskanje...",
            },
        },
        columns: ['title','izbris'],
    },
    methods: {
        //funkcije za obvestila
        get_helps: function() {
            axios
            .get("/api/shared/get_help/", false)
            .then((response) => {
                this.tableData=response.data;
            })
            .catch(error => {
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                alert("Napaka pri pridobivanju podatkov");
              }
            });
          },
          get_help(helpId) {
            let help = this.tableData.filter(help => help.id === helpId)[0];
            this.currentHelp = {
              id: help.id,
              title: help.title,
              description: help.description
            }
          },
          delete_help(helpId) {
            var result = confirm("Ste prepričani, da želite izbrisati pomoč?");
                if (result) {
                var headers = {
                  'X-CSRFToken': getCookie("csrftoken"),
                }
                axios
                .delete("/api/upravitelj_tekmovanj/delete_help/?helpId="+helpId, {headers:headers})
                .then((response) => {
                  this.tableData = this.tableData.filter(x => x.id !== helpId);
                })
                .catch(error => {
                if(error.response.status == 404) {
                    alert("Šola ne obstaja");
                }
                if(error.response.status == 409) {
                  alert("Manjkajoči parametri")
                }
                if(error.response.status == 403) {
                  alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
                }
                if(error.response.status == 500) {
                    console.log("Something went wrong");
                }
                });
            }
          },
    }
});
app.get_helps();
