Vue.use(VueTables.ClientTable);

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      currentMessage: {id:"",title:"",message:"",email:""},
      readMessages: [],
      tableData: [
      ],
      options: {
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
      columns: ['prebrano', 'title','created_at','izbris'],
    },
    methods: {
      get_messages: function() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
       axios
        .get("/api/upravitelj_tekmovanj/get_messages/", false,{headers: headers})
        .then((response) => {
          this.tableData=response.data;
          this.readMessages = response.data.filter(message => message.read);
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Napaka pri pridobivanju sporočil");
          }
        });
      },

      toggle_read_message(messageId) {
          let markAsRead = false;
          this.readMessages.forEach(function (message) {if(message.id === messageId) {markAsRead = true;}}, this);

          var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
          };

          axios
              .post("/api/upravitelj_tekmovanj/toggle_read_message/", {messageId, 'read': markAsRead}, {headers:headers})
              .then((response) => {
                  let title = '';
                  let message = '';
                  if(markAsRead) {
                      title = 'Sporočilo prebrano';
                      message = 'Sporočilo označeno kot prebrano';
                  } else {
                      title = 'Sporočilo neprebrano';
                      message = 'Sporočilo označeno kot neprebrano';
                  }
                  this.$bvToast.toast(message, {
                      title: title,
                      autoHideDelay: 3000,
                      appendToast: true,
                      variant: 'success',
                  });
              })
              .catch(error => {
                  if(error.response.status == 409) {
                      alert("Manjkajoči ali nepravilni parametri");
                  }
                  if(error.response.status == 403) {
                      alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
                  }
                  if(error.response.status == 404) {
                      alert("Sporočilo ne obstaja");
                  }
                  if(error.response.status == 500) {
                      console.log("Something went wrong");
                  }
              })
      },

      delete_message(messageId) {
        var result = confirm("Ste prepričani, da želite izbrisati sporočilo?");
            if (result) {
            var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
            axios
            .delete("/api/upravitelj_tekmovanj/delete_message/?messageId="+messageId, {headers:headers})
            .then((response) => {
              this.tableData = this.tableData.filter(x => x.id !== messageId);
            })
            .catch(error => {
            if(error.response.status == 404) {
                alert("Šola ne obstaja");
            }
            if(error.response.status == 409) {
              alert("Manjkajoči parametri")
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
                console.log("Something went wrong");
            }
            });
        }
      },
      get_message(idMessage) {
          let message = this.tableData.filter(msg => msg.id === idMessage)[0];
          this.currentMessage = {
              id: message.id,
              title: message.title,
              message: message.message,
              email: message.email
          }
      },
      downloadCsv(text, fileType, fileName) {
        var blob = new Blob([text], { type: fileType })
        var a = document.createElement('a');
        a.download = fileName;
        a.href = URL.createObjectURL(blob);
        a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
      },
      toCsv: function() {
        let newTable=this.tableData.filter(el => el !== null);
        this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
      },
    },
  });

app.get_messages();
