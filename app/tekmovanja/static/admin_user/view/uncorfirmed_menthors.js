Vue.use(VueTables.ClientTable);
var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      selectedUser:"",
      user_schools: "",
      tekmovanja: "",
      loadingSelectedUser: false,
      loadingUserSchools: false,
      loadingTekmovanja: false,
      columns: ['email','naziv','Potrdi'],
      tableData: [
      ],
      options: {
        texts: {
          filterPlaceholder:"Iskanje...",
        },
      },
    },
    methods: {
      userConfirm: function(event,userId,schoolId) {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }

        axios
        .post("/api/shared/confirm_user/", {userId:userId,schoolId:schoolId},{headers: headers})
        .then((response) => {
          window.location.reload();
        })
        .catch(error => {
          if(error.response.status == 400) {
            alert(error.response.data);
          }
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            console.log("Something went wrong");
          }
        });
      },

      getMentors: function() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        axios
        .get("/api/upravitelj_tekmovanj/get_uncorfirmed_menthors/",false,{ headers: headers } )
        .then((response) => {
          if(response.status == 200) {
            this.tableData = JSON.parse(response.data);
          }
        })
        .catch(error => {
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Napaka pri pridobivanju mentorjev");
          }
        });
      },
      getUser(userId) {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }

        this.selectedUser = {};
        this.loadingSelectedUser = true;
        axios
        .get("/api/upravitelj_tekmovanj/get_user_by_id/",{params: { userId:userId}},{headers:headers})
        .then((response) => {
          this.loadingSelectedUser = false;
          if(response.status == 200) {
            this.selectedUser = response.data;
          }
        })
        .catch(error => {
          this.loadingSelectedUser = false;
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Pri pridobivanju podatkov za uporabnika, je prišlo do napake, podatki v pojavnem oknu ne bodo pravilni");
          }
        });
      },

      getUserSchoolsSubscriptions(userId) {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }

        this.user_schools = [];
        this.loadingUserSchools = true;
        axios
        .get("/api/upravitelj_tekmovanj/user_school_subscripitons_by_id/",{params: { userid:userId}},{headers:headers})
        .then((response) => {
          this.loadingUserSchools = false;
          if(response.status == 200) {
            this.user_schools = response.data;
          }
        })
        .catch(error => {
          this.loadingUserSchools = false;
          if(error.response.status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(error.response.status == 500) {
            alert("Pri pridobivanju podatkov za uporabnikove šole, je prišlo do napake, podatki v pojavnem oknu ne bodo pravilni");
          }
        });
      },

    getUserCompetitions(userId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }

      this.tekmovanja = [];
      this.loadingTekmovanja = true;
      axios
      .get("/api/upravitelj_tekmovanj/admin_view_user_competitions/",{params: { userid:userId}},{headers:headers})
      .then((response) => {
        this.loadingTekmovanja = false;
        if(response.status == 200) {
          this.tekmovanja = response.data;
        }
      })
      .catch(error => {
        this.loadingTekmovanja = false;
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          alert("Pri pridobivanju podatkov za uporabnikova tekmovanja, je prišlo do napake, podatki v pojavnem oknu ne bodo pravilni");
        }
      });
    },
    downloadCsv(text, fileType, fileName) {
      var blob = new Blob([text], { type: fileType })
      var a = document.createElement('a');
      a.download = fileName;
      a.href = URL.createObjectURL(blob);
      a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
      a.style.display = "none";
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
    },
    toCsv: function() {
      let newTable=this.tableData.filter(el => el !== null);
      this.downloadCsv(Papa.unparse(newTable),'csv','MentorsCsv');
    },
      handler(userId) {
        this.getUser(userId);
        this.getUserCompetitions(userId);
        this.getUserSchoolsSubscriptions(userId);
      }
    },
  });
app.getMentors();
