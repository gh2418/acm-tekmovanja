var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      email: "",
      status: ""
    },
    methods: {
      send_lost_password_email: function() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        let data = {
            'email': this.email,
        };
        this.status="Pošiljanje...";
        axios
        .post("/api/guest/send_lost_password_email/", data, {headers:headers})
        .then((response) => {
          this.status="V primeru, da elektronski naslov, ki ste ga vnesli, v sistemu obstaja, bo elektronsko sporočilo za zamenjavo gesla poslano na vnešen elektronski naslov.";
        })
        .catch(error => {
          this.status = "Prišlo je do napake";
        });
      }
    },
  });
