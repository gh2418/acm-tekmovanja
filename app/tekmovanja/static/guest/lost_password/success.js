var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
        status: "",
        password: "",
        repeatPassword: "",
    },
    methods: {
      send_token: function() {
        token=document.getElementById("token").innerHTML;
        let data = {
            'token': token,
            'password':  this.password,
            'repeatPassword':  this.repeatPassword,
        };
        this.status="Pošiljanje...";
        axios
        .post("/api/guest/send_lost_password_token/", data)
        .then((response) => {
            this.status="Geslo uspešno spremenjeno.";
        })
        .catch(error => {
            if(error.response.status == 404) {
                window.location.replace("/lost_password_verify/"+this.token);
            }
            if(error.response.status==400 && error.response.data =='passwordNotMatching') {
                this.status="Geslo se ne ujema. Geslo ni bilo spremenjeno.";
            }
            else if(error.response.status==400 && error.response.data =='passwordTooLowChars') {
                this.status="Geslo mora vsebovati vsaj 5 znakov.";
            }
            else if(error.response.status==400) {
                window.location.replace("/lost_password_verify/"+this.token);
            }
        });
      }
    },
  });
