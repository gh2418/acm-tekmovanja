 Vue.use(Autocomplete)
 var app = new Vue({
    delimiters: ['[[', ']]'],
    el: "#container",
    data: {
        schools: "",
        currentInput: "",
        izbrane_sole: [],
        status: "",
        ime: "",
        priimek: "",
        up_ime: "",
        email: "",
        geslo: "",
        geslo_ponovno: "",
        posiljanje_status: "",
        checked: false,
    },
    methods: {
        validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
        getSchools: function() {
            axios
            .get("/api/guest/get_seznam_naziv_sol_call/", false)
            .then((response) => {
            if(response.status == 200) {
                this.schools=response.data;
            }
            })
            .catch(error => {
            if(error.response.status == 403) {
            }
            if(error.response.status == 500) {
            }
            });
        },

        add_school() {
            this.currentInput = document.getElementById("vnos_sole").value;

            if (this.izbrane_sole.includes(this.currentInput)) {
                this.status = "Šolo ste že izbrali";
                return;
            }
            if (!this.schools.includes(this.currentInput)) {
                this.status = "Šole ni na seznamu šol";
                return;
            }
            this.status = "";
            this.izbrane_sole.push(this.currentInput);
        },

        delete_school() {
            this.izbrane_sole.pop();
        },

        search(input) {
            if (input.length < 1) { return [] }
            return this.schools.filter(school => {
              return school.toLowerCase()
                .includes(input.toLowerCase())
            })
          },

        sendRegistrationRequest() {
            var headers = {
                'X-CSRFToken': getCookie("csrftoken"),
            }
            this.posiljanje_status = "Pošiljanje ...";
            let data = {
                'Ime': this.ime,
                'Priimek': this.priimek,
                'UpIme': this.up_ime,
                'email': this.email,
                'sola': this.izbrane_sole,
                'geslo': this.geslo,
                'izbrane_sole': this.izbrane_sole,
                'pogoji_uporabe': this.checked
            };

            axios
            .post("/api/guest/registration_call/", data, {headers: headers})
            .then((response) => {
                if(response.status === 201) {
                    this.posiljanje_status = "Registracija uspešna. Za uspešno prijavo morate potrditi račun, preko povezave, ki je bila poslana na Vaš email račun. Pomembno: račun morate potrditi v roku 1 ure sicer ga ne bo več mogoče potrditi.";
                }
                if(response.status === 409) {
                    this.posiljanje_status = "";
                    this.status = "Napaka.";
                }
            })
            .catch(error => {
                if(error.response.status === 400) {
                    switch(error.response.data) {
                        case 'email_zaseden' : this.posiljanje_status = ""; this.status = "Email naslov zaseden"; break;
                        case 'up_ime_zasedeno': this.posiljanje_status = ""; this.status = "Uporabniško ime zasedeno"; break;
                        case 'pogoji_uporabe': this.posiljanje_status = ""; this.status = "Za uspešno registracijo se morate strinjati s pogoji uporabe"; break;
                        case 'up_ime_vsebuje_presledke': this.posiljanje_status = ""; this.status = "Uporbniško ime ne sme vsebovati presledkov pred ali za uporabniškim imenom."; break;
                        case 'neveljaven_elektronski_naslov': this.posiljanje_status = ""; this.status = "Elektronski naslov je neveljaven."; break;
                        case 'geslo_premalo_znakov': this.posiljanje_status = ""; this.status = "Geslo mora vsebovati vsaj 5 znakov."; break;
                        case 'no_mail_sent': this.posiljanje_status = ""; this.status = "V sistem ste se registrirali vendar je prišlo do problema pri pošiljanju elektronskega sporočila, " +
                                    "preko katerega lahko aktivirate svoj račun (prijava ne bo uspešna). Prosimo pošljite elektronsko sporočilo na ekipo ACM," +
                                    " da vam račun aktivirajo (sporočilo pošljite iz elektronskega naslova, ki ste ga uporabili pri registraciji.). Prav tako se lahko poizkusite ponovno registrirati čez kakšen dan."; break;
                    }
                }
            });
        },

        registration() {
            if(this.ime.length == 0 || this.priimek.length == 0 || this.up_ime.length == 0 || this.email.length == 0) {
                this.status = "Vnesti morate vse parametre ki so označeni z obvezno";
                return
            }
            if(this.geslo.length < 5) {
                this.status = "Geslo mora biti dolgo vsaj 5 znakov.";
                return
            }
            if(this.geslo !== this.geslo_ponovno) {
                this.status = "Gesli se ne ujemata";
                return
            }
            if(!this.validateEmail(this.email)) {
                this.status= "Napačen elektronski naslov";
                return
            }

            if(!this.checked) {
                this.status = "Za uspešno registracijo se morate strinjati s pogoji uporabe.";
                return;
            }
            
            this.status = "";
            this.sendRegistrationRequest();
        }
    },
  });
app.getSchools();
