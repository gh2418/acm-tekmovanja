var mentorLoader = new Vue({
    delimiters: ['[[', ']]'],
    el: '#mentor-loader',
    data() {
      return {
        mounted: false
      }
    },

    methods: {
        stop: function() {
            this.mounted = true;
        }
    },

    template: '<div v-if=!mounted class="prerender-loader" style="visibility: visible"></div>',
});