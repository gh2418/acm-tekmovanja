var headers = {
    'X-CSRFToken': getCookie("csrftoken"),
}

function show_original_smtp_password() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        let successElement = document.querySelector('#get_original_password_success_result');
        let errorElement = document.querySelector('#get_original_password_error_result');

        successElement.innerHTML = "";
        errorElement.innerHTML = "";


        if (this.readyState == 4 && this.status == 200) {
            let responseObject = JSON.parse(this.response);
            if(responseObject.password_match) {
                successElement.innerHTML = 'Ujemanje!';
            } else {
                successElement.innerHTML = 'Neujemanje.';
            }
        }

        if (this.readyState == 4 && this.status == 400) {
            successElement.innerHTML = "Ta funkcionalnost ne deluje pravilno napaka: " + this.response;
        }
    };
    xhttp.open("POST", "/api/superuser/is_correct_smtp_password/", true);

    xhttp.setRequestHeader('X-CSRFToken', getCookie("csrftoken"));
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send('smtpPassword='+document.querySelector('#user_password').value);
}

function showForm() {
    let form = document.getElementById("show_password_form");
    if (form.style.display === "none") {
        form.style.display = "block";
    } else {
        form.style.display = "none";
    }
}