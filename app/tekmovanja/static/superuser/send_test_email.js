var headers = {
    'X-CSRFToken': getCookie("csrftoken"),
}
function sendTestEmail() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        let successResultElement = document.querySelector('#send_test_email_success_result');
        let errorResultElement = document.querySelector('#send_test_email_error_result');

        successResultElement.innerHTML = "";
        errorResultElement.innerHTML = "";


        if (this.readyState == 4 && this.status == 200) {
            successResultElement.innerHTML = "Sporočilo uspešno poslano, nastavitve so pravilno nastavljene.";
        }

        if (this.readyState == 4 && this.status == 400) {
            let responseObject = JSON.parse(this.response);
            let msg = "";
            switch (responseObject.response) {
                case "email_naslov_ni_vnesen": msg = "Vnesti morate elektronki naslov."; break;
                case "brez_nastavitev": msg = "V sistemu ni nobenih nastavitev za pošiljanje elektronske pošte, dodajte nastavitev."; break;
                case "napaka_pri_posiljanju": msg = "Pri pošiljanju je prišlo do NAPAKE. Pošiljanje emailov znotraj aplikacije ne deluje. PREVERITE nastavitve."; break;
            }

            errorResultElement.innerHTML = msg;
        }
    };
    xhttp.open("POST", "/api/superuser/send_test_mail/", true);

    xhttp.setRequestHeader('X-CSRFToken', getCookie("csrftoken"));
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhttp.send('testEmail='+document.querySelector('#test_email').value);
}