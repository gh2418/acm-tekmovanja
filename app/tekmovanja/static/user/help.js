var app = new Vue({
    delimiters: ['[[', ']]'],
    el: "#container",
    data: {
        help: ""
    },
    methods: {
        //funkcije za obvestila
        get_help: function() {
            axios
            .get("/api/shared/get_help/", false)
            .then((response) => {
                this.help = response.data;
            })
            .catch(error => {
              if(error.response.status == 403) {
                alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
              }
              if(error.response.status == 500) {
                console.log("Something went wrong");
              }
            });
          },
        }
});
app.get_help();
