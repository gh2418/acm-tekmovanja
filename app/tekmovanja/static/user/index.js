
Vue.use(VueTables.ClientTable);
var user = new Vue({
  delimiters: ['[[', ']]'],
  el: "#container",
  data: {
    user: "",
    unconfirmedMenthors: false,
    newCompetitions: false,
  },
  methods: {
    onInit() {
      if(localStorage.getItem('user')) {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.hasNewCompetitions();
        this.hasUnconfirmedMenthors();
      } else {
        this.get_user_and_change_last_login();
      }
    },
    get_user_and_change_last_login: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/user/get_user_and_change_last_login/", false, {headers:headers})
      .then((response) => {
        if(response) {
            localStorage.setItem('user', JSON.stringify(response.data.user));
            this.user = response.data.user;
            this.hasNewCompetitions();
            this.hasUnconfirmedMenthors();
          }
      })
      .catch(error => {
        alert("Nekaj je šlo narobe prosim prijavite se ponovno");
      });
    },

    //funkcije za obvestila
    hasNewCompetitions: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/user/new_competitions/", {params: { last_login: this.user.last_login}},{headers:headers})
      .then((response) => {
        if(response.status == 200) {
          this.newCompetitions = true;
          this.lastLoginUpdate();
        }
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },

    hasUnconfirmedMenthors: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/user/user_has_unconfirmed_mentors/",{params: { last_login: this.user.last_login}},{headers:headers})
      .then((response) => {
        this.unconfirmedMenthors = true;
      })
      .catch(error => {
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
          console.log("Something went wrong");
        }
      });
    },
  },
});
user.onInit();