var message = new Vue({
  delimiters: ['[[', ']]'],
  el: "#container",
  data: {
    title:"",
    message:"",
    status: "",
    maxCountMessage: 150,
    remainingCountMessage: 150,
    maxCountTitle: 40,
    remainingCountTitle: 40,
    statusSuccess : false,
    statusNotSuccess: false,
  },
  methods: {
    sendMessage() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        data = {
            title: this.title,
            message: this.message,
        }
        if (confirm('Ste prepričani, da želite poslati sporočilo?')) {
          axios
          .post("/api/user/send_message/", data,{headers: headers})
          .then((response) => {
            this.statusNotSuccess = false;
            this.statusSuccess = true;
            this.status = "Sporočilo uspešno poslano";
          })
          .catch(error => {
            if(error.response.status == 400) {
                this.statusSuccess = false;
                this.statusNotSuccess = true;
                if (error.response.data === 'predolgo sporočilo ali naslov') {

                    this.status = "Predolgo sporočilo ali naslov. Naslov(največ 50 znakov), sporočilo(nejveč 150 znakov).";
                }
                if (error.response.data === 'sporočilo ali naslov prazno') {
                    this.status = "Naslov ali sporočilo prazno. Obvezni so vsi podatki."
                }
                if (error.response.data === 'preveč_sporočil') {
                  this.status = "Presegli ste zgornji limit za sporočila (10). Prosimo počakajte, da upravitelj tekmovanj Vaša sporočila pregleda.";
              }
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
                alert("Napaka");
            }
          });
        }
      },
      countdownMessage: function() {
        this.remainingCountMessage = this.maxCountMessage - this.message.length;
      },
      countdownTitle: function() {
        this.remainingCountTitle = this.maxCountTitle - this.title.length;
     }


  },
});

