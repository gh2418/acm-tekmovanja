Vue.use(VueTables.ClientTable,Autocomplete);
var mentorLoader = mentorLoader;

var user_schools = new Vue({
  delimiters: ['[[', ']]'],
  el: "#container",
  data: {
    loadingSchoolUsers: false,
    schoolModalStatus: "",
    selectedSchoolId: -1,
    selectedSchoolName: "",
    //moje šole
    userSchools: [],
    requestGetUserSchoolsPerformed: false,
    sole: "",
    status: "",
    columns: ['Status','Naziv_mentor','Potrdi'],
    tableData: [],
    options: {
      headings: {
        Status: ''
      },
      texts: {
        filterPlaceholder:"Iskanje po mentorjih",
      },
    },
    sortable: ['Naziv','Status'],
    filterable: ['Naziv','Status'],
    add_status: "",
    add_schools: "",
    currentInput: "",
    izbrane_sole: [],
  },
  
  mounted: function () {
      this.$nextTick(function () {
        document.getElementById('container').style.visibility = 'visible';
        const elements = document.querySelectorAll('#container');
        elements.forEach(function(element) {element.style.visibility = 'visible';})
        mentorLoader.stop();
      })
  },

  methods: {

    //modul za nepotrjene
    currentSelectedSchoolUnconfirmed(schoolId,schoolName) {
      this.selectedSchoolId = schoolId;
      this.selectedSchoolName = schoolName;
    },

    //modul za potrjene
    currentSelectedSchoolConfirmed(schoolId,schoolName) {
      this.selectedSchoolId = schoolId;
      this.selectedSchoolName = schoolName;
      this.getSchoolUsers(schoolId);
    },

    //pridobi uporabnikove šole
    getUserSchools: function() {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .get("/api/user/user_school_subscripitons/",false,{headers:headers})
      .then((response) => {
        this.requestGetUserSchoolsPerformed = true;
        if(response.status == 200) {
          this.userSchools = response.data;
        }
      })
      .catch(error => {
        this.requestGetUserSchoolsPerformed = true;
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
            alert("Napaka pri pridobivanju vaših šol");
        }
      });
    },

    //pridobi vse uporabnike na šoli
    getSchoolUsers: function(id) {
      this.tableData = [];
      this.schoolModalStatus = "";
      var headers = {
            'X-CSRFToken': getCookie("csrftoken"),
          }
      this.loadingSchoolUsers = true;
      axios
      .get("/api/user/school_users/", {params: { schoolId:id}},{headers: headers})
      .then((response) => {
        this.loadingSchoolUsers = false;
        this.tableData = response.data.user_schools;
      })
      .catch(error => {
        this.loadingSchoolUsers = false;
        if(error.response.status == 400) {
            switch(error.response.data) {
                case 'user_unconfirmed': this.schoolModalStatus = "Na tej šoli niste potrjeni, zato ne morete videti podatkov o drugih mentorjih na šoli. Obvestite administratorja."; break;
                case 'nepravilni ali manjkajoči parametri': this.schoolModalStatus = "Napaka. Pri pošiljanju zahtevka je prišlo do napake (parametri), prikazanih ni nobenih podatkov. Obvestite administratorja."; break;
                default: this.schoolModalStatus = 'Prišlo je do napake, prikazanih ni nobenih podatkov, obvestite administratorja.';
             }
        }
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 404) {
             switch(error.response.data) {
                case 'school_not_exists': this.schoolModalStatus = "Napaka (šola ne obstaja), prikazanih ni nobenih podatkov. Obvestite administratorja."; break;
                case 'user_not_exists': this.schoolModalStatus = "Napaka, na tej šoli ne učite, prikazanih ni nobenih podatkov. Obvestite administratorja."; break;
                default: this.schoolModalStatus = 'Prišlo je do napake, prikazanih ni nobenih podatkov, obvestite administratorja.';
             }
        }
        if(error.response.status == 500) {
            this.schoolModalStatus = "Pri pridobivanju podatkov o uporabnikih na šoli je šlo nekaj narobe, zaprite in ponovno odprite pojavno okno, v kolikor je problem še vedno prisoten obvestite admina.";
        }
        if(this.schoolModalStatus === "") {
            this.schoolModalStatus = "Pri pridobivanju podatkov o uporabnikih na šoli je šlo nekaj narobe, obvestite administratorja.";
        }
      });
    },

    //izbriši se iz šole
    school_unregistration(schoolId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      if (confirm('Ste prepričani, da se želite izbrisati iz šole?')) {
        axios
        .post("/api/user/school_unregistration/", {schoolId:schoolId},{headers: headers})
        .then((response) => {
          window.location.reload();
        })
        .catch(error => {
          let status = error.response.status;
          if(status == 403) {
            alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
          }
          if(status == 409 || status == 500) {
            alert("Napaka");
          }
        });
      }
    },

    //potrdi uporabnika na neki šoli
    userConfirm: function(event,userId,schoolId) {
      var headers = {
        'X-CSRFToken': getCookie("csrftoken"),
      }
      axios
      .post("/api/shared/confirm_user/", {userId:userId,schoolId:schoolId},{headers: headers})
      .then((response) => {
        let button = document.getElementById(event.target.id);
        button.style="display:none";
        this.$bvToast.toast('Uporabnik uspešno potrjen', {
          title: 'Uspešno potrjen',
          autoHideDelay: 3000,
          appendToast: true,
          variant: 'success',
        });

        //zamenjaj vrstico, da je že potrjen
        this.tableData.forEach(el => {
          if(el.ID_Mentor == userId) {
            el.Ze_potrjen=1;
          }
        })

        //zamenjaj gumb za ikono
        let status = button.parentElement.parentElement.parentElement.childNodes[2];
        status.innerHTML='<img src="/static/icons/checked.png" class="slika-potrdi">';


        //če je uporabnik postal potrjen zamenjaj ikono
        if(response.status == 202) {
          let status = button.parentElement.parentElement.parentElement.childNodes[0];
          status.style="color:green";
          status.innerHTML='<img src="/static/icons/verified-account.png">';
        }
      })
      .catch(error => {
        if(error.response.status == 400) {
          alert(error.response.data);
        }
        if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
        }
        if(error.response.status == 500) {
            alert("Napaka pri potrjevanju");
        }
      });
    },

    //dodajanje šol
    //pridobi vse šole za filtriranje
    getSchools: function() {
      axios
      .get("/api/guest/get_seznam_naziv_sol_call/", false)
      .then((response) => {
      if(response.status == 200) {
          this.add_schools=response.data;
      }
      })
      .catch(error => {
      if(error.response.status == 403) {
        alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
      }
      if(error.response.status == 500) {
         alert("Napaka pri pridobivanju vseh šol");
      }
      });
  },

  //dodaj šolo med dodane šole
  add_school() {
      this.currentInput = document.getElementById("vnos_sole").value;

      if (this.izbrane_sole.includes(this.currentInput)) {
          this.status = "Šolo ste že izbrali";
          return;
      }
      if (!this.add_schools.includes(this.currentInput)) {
          this.status = "Šole ni na seznamu šol";
          return;
      }
      this.status = "";
      this.izbrane_sole.push(this.currentInput);
  },

  //izbriši šolo
  delete_school() {
      this.status = "";
      this.izbrane_sole.pop();
  },

  search(input) {
      if (input.length < 1) { return [] }
      return this.add_schools.filter(school => {
        return school.toLowerCase()
          .includes(input.toLowerCase())
      })
  },

  //zahteva za dodajanje izbranih šol
  user_add_schools() {
     if(this.izbrane_sole.length === 0) {
         this.status = "Nobena šola ni bila izbrana";
         return;
     }
     var headers = {
       'X-CSRFToken': getCookie("csrftoken"),
     }
     let data = {
         'izbrane_sole': this.izbrane_sole
     };
     axios
     .post("/api/user/user_add_schools/", data,{headers:headers})
     .then((response) => {
         if(response.status === 201) {
             this.add_status = "Šole uspešno dodane";
             location.reload();
         }
     })
     .catch(error => {
         if(error.response.status == 403) {
          alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
         }
         if(error.response.status == 409) {
            alert("Napaka pri dodajanju šole (parametri).");
         }
         if(error.response.status == 500) {
            alert("Napaka");
         }
     });
 }
},
});
user_schools.getSchools();
user_schools.getUserSchools();
