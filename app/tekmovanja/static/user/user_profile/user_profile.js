var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#container',
    data: {
      ime: "",
      priimek: "",
      up_ime: "",
      email: "",
      status: "",
      geslo_status: "",
      geslo1: "",
      geslo2: "",
      user: "",
    },
    mounted: function () {
      this.$nextTick(function () {
        document.getElementById('container').style.visibility = 'visible';
        const elements = document.querySelectorAll('#container');
        elements.forEach(function(element) {element.style.visibility = 'visible';})
        mentorLoader.stop();
      })
    },
    methods: {
      onInit() {
        if(localStorage.getItem('user')) {
          this.user = JSON.parse(localStorage.getItem('user'));
        } else {
          this.get_user_info();
        }
        this.ime = this.user.first_name;
        this.priimek = this.user.last_name;
        this.up_ime = this.user.username;
        this.email = this.user.email;
      },

      validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
      },

      get_user_info: function() {
        var headers = {
          'X-CSRFToken': getCookie("csrftoken"),
        }
        axios
        .get("/api/user/get_user_info/", false, {headers:headers})
        .then((response) => {
          if(response) {
              localStorage.setItem('user', JSON.stringify(response.data.user));
              this.user = JSON.parse(localStorage.getItem('user'));
            }
        })
        .catch(error => {
            let status = error.response.status;
            if(status == 409) {
                alert("Nekaj je šlo narobe prosimo prijavite se ponovno");
            }
            if(status == 500) {
                alert("Napaka pri pridobivanju podatkov");
            }
        });
      },

      change_data() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        let data = {
          'Ime': this.ime,
          'Priimek': this.priimek,
          'UpIme': this.up_ime,
          'email': this.email,
        };
        if (!this.validateEmail(this.email)) {
          this.status="Napačen elektronski naslov";
          return;
        }
        if(this.ime == this.user.first_name && this.priimek == this.user.last_name && this.up_ime == this.user.username && this.email == this.user.email) {
          this.status="Spremenili niste nobenega podatka.";
          return;
        }
        if (confirm('Ste prepričani, da želite spremeniti podatke?')) {
          this.status = "Pošiljanje ..."
          axios
          .post("/api/user/user_change_profile/", data, {headers: headers})
          .then((response) => {
            if(response.status === 201) {
              this.status = "Podatki so bili uspešno spremenjeni. Da Vam bodo podatki vidni se boste morali ponovno prijaviti v sistem. Če ste spremenili svoj elektronski naslov ga morate za uspešno zamenjavo potrditi.";
            }
          })
          .catch(error => {
            if(error.response.status == 400) {
              switch(error.response.data) {
                case 'up_ime_vsebuje_presledke': this.posiljanje_status = ""; this.status = "Uporbniško ime ne sme vsebovati presledkov pred ali za uporabniškim imenom."; break;
                case 'neveljaven_elektronski_naslov': this.posiljanje_status = ""; this.status = "Elektronski naslov je neveljaven"; break;
             }
            }
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 407) {
              this.status = "Uporabniško ime ne sme vsebovati znaka @";
            }
            if(error.response.status == 409) {
              switch(error.response.data) {
                case 'mankajoci_podatki': this.status = "Manjkajoči podatki"; break;
                case 'predolgo_up_ime_ali_email': this.status = "Uporabniško ime ali email sta predolga"; break;
                case 'up_ime_zasedeno': this.status = "Uporabniško ime zasedeno"; break;
                case 'email_zaseden': this.status = "Email naslov je že zaseden"; break;
              }
            }
            if(error.response.status == 500) {
                alert("Napaka");
            }
          });
        }
      },

      change_password() {
        if (this.geslo1 !== this.geslo2) {
          this.geslo_status = "Gesli se ne ujemata";
          return;
        }
        if (this.geslo1.length < 5) {
          this.geslo_status = "Geslo mora biti dolgo vsaj 5 znakov";
          return;
        }
        console.log(this.geslo1)
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        let data = {
          'geslo': this.geslo1,
        };

        if (confirm('Ste prepričani, da želite spremeniti geslo?')) {
          this.status = "Pošiljanje ..."
          axios
          .post("/api/user/user_change_password/", data, {headers: headers})
          .then((response) => {
            if(response.status === 201) {
              this.geslo_status = "Geslo uspešno spremenjeno. Pri ponovni prijavi v sistem morate vnesti novo geslo.";
            }
          })
          .catch(error => {
            if(error.response.status === 409) {
              this.geslo_status = "Napaka.";
            }
            if(error.response.status == 500) {
                alert("Napaka");
            }
          });
        }
      },

      delete_user() {
        var headers = {
              'X-CSRFToken': getCookie("csrftoken"),
            }
        if (confirm('Ste prepričani, da se želite izbrisati?')) {
          axios
          .post("/api/user/delete_user/", false,{headers: headers})
          .then((response) => {
            window.location.replace("/login");
          })
          .catch(error => {
            if(error.response.status == 403) {
              alert("Nekaj je šlo narobe. Odjavite in prijavite se ponovno.");
            }
            if(error.response.status == 500) {
                alert("Napaka");
            }
          });
        }
      }
    }
  });

  app.onInit();
