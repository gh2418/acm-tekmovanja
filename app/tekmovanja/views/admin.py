from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.shortcuts import redirect
from rest_framework import generics

#here are all templates for admin

def checkAdminPermissions(user):
    return user.is_authenticated and user.is_staff and user.is_active and not user.is_superuser

class admin_index(generics.RetrieveAPIView):

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response(template_name='admin_user/index.html')
        else:
            return redirect('login')

class admin_add_schools(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'add_schools':'Active'}, template_name='admin_user/add/schools.html')
        else:
            return redirect('login')

class admin_bulk_schools(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'bulk_schools':'Active'}, template_name='admin_user/add/bulk_schools.html')
        else:
            return redirect('login')

class admin_add_competitions(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'add_competitions':'Active'}, template_name='admin_user/add/competitions.html')
        else:
            return redirect('login')

class admin_add_competition_type(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'add_competition_type':'Active'}, template_name='admin_user/add/competition_type.html')
        else:
            return redirect('login')

class admin_view_competition_type(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'view_competition_type':'Active'}, template_name='admin_user/view/competition_type.html')
        else:
            return redirect('login')

class admin_add_admin(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'add_admin':'Active'}, template_name='admin_user/add/admin.html')
        else:
            return redirect('login')

@permission_classes((IsAuthenticated,))
class admin_send(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'admin_send':'Active'}, template_name='admin_user/view/admin_send.html')
        else:
            return redirect('login')

class admin_view_competitions(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'view_competitions':'Active'}, template_name='admin_user/view/competitions.html')
        else:
            return redirect('login')

class admin_view_schools(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'view_schools':'Active'}, template_name='admin_user/view/schools.html')
        else:
            return redirect('login')

class admin_view_users(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'view_users':'Active'}, template_name='admin_user/view/mentors.html')
        else:
            return redirect('login')

class admin_view_help(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'view_help': 'Active'}, template_name='admin_user/view/help.html')
        else:
            return redirect('login')

class admin_edit_profile(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'edit_profile': 'Active'}, template_name='admin_user/view/edit_profile.html')
        else:
            return redirect('login')

class admin_add_help(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'add_help': 'Active'}, template_name='admin_user/add/help.html')
        else:
            return redirect('login')

class uncorfirmed_menthors(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'unconfirmed_menthors': 'Active'}, template_name='admin_user/view/uncorfirmed_menthors.html')
        else:
            return redirect('login')

class first_unconfirmed(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'first_unconfirmed': 'Active'}, template_name='admin_user/view/first_unconfirmed.html')
        else:
            return redirect('login')

class messages(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'msg': 'Active'}, template_name='admin_user/view/messages.html')
        else:
            return redirect('login')


class documentation(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkAdminPermissions(request.user):
            return Response({'documentation': 'Active'}, template_name='admin_user/documentation/swagger.html')
        else:
            return redirect('login')
