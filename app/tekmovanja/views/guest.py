from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.shortcuts import redirect
from tekmovanja.serializers import *
from rest_framework import generics
from tekmovanja.models import *
from tekmovanja.lib.send_mail_to_user import send_mail_to_user
from django.conf import settings
from site_config import settings as project_settings
from django.contrib import auth
import datetime
import random
import string

import os
import datetime
import json

#...........................................................
# HTML-ji ZA GOSTA
#...........................................................

@permission_classes([])
class login_view(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of login page.
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                return HttpResponseRedirect('/admin/')
            if request.user.is_staff:
                return HttpResponseRedirect('/admin_index/')
            else:
                return HttpResponseRedirect('/user_index/')

        return Response({'response':"", "username":"","password":""}, template_name='guest/login.html')

    def post(self, request):
        response = "Napačno uporabniško ime ali geslo"
        try: 
            username = request.data["username"]
            password = request.data["password"]
        except:
            response = "Vnesite vse podatke"
            return Response({'response':response}, template_name='guest/login.html')
        
        if User.objects.filter(username=username, is_superuser=1, is_staff=1).exists():
            return Response({'response':response, "username":username, "password":""}, template_name='guest/login.html')

        user = authenticate(username=username, password=password)

        if user is not None:
            auth.login(request,user)
            if request.user.is_staff:
                return HttpResponseRedirect('/admin_index/')
            else:
                return HttpResponseRedirect('/user_index/')
        else:
            return Response({'response':response, "username":username, "password":""}, template_name='guest/login.html')
    

@permission_classes([])
class root_view(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of login page.
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):

        return Response( template_name='guest/root_view.html')


class registration_view(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of registration page.
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response(template_name='guest/registration.html')

class verifikacija(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request,token):
        try:
            token = Verifikacija.objects.get(token=token)
        except:
            return Response(template_name='guest/unconfirmed.html')
        usr = User.objects.get(id=token.user_id)
        naive = token.created_at.replace(tzinfo=None)
        seconds_from_registration=datetime.datetime.now().timestamp()-naive.timestamp()

        if(usr.is_active != True):
            if(seconds_from_registration > 3600):
                Uci.objects.filter(ID_Mentor=usr.id).delete()
                Verifikacija.objects.filter(user_id=usr.id).delete()
                usr.delete()
                return Response(template_name='guest/unconfirmed.html')
            else:
                usr = User.objects.get(id=token.user_id)
                usr.is_active = True
                usr.save()
                message="""
                Pozdravljeni.
                V spletni aplikaciji ACM tekmovanja ste uspešno potrdili svoj elektronski račun, prijava v sistem je sedaj mogoča. 
                V aplikacijo """+project_settings.BASE_URL+""" se lahko sedaj prijavite z uporabniškim imenom in geslom, ki ste ga ob registraciji vnesli. 

                Vaše uporabniško ime: """+ usr.username + """


                Ekipa ACM
                """

                send_mail_to_user('ACM username', message, [usr.email], False)
                return Response(template_name='guest/confirmed.html')
        else:
            return Response(template_name='guest/already-confirmed.html')

class confirmed(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response(template_name='guest/confirmed.html')

class unconfirmed(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response(template_name='guest/unconfirmed.html')

class pogoji_uporabe(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response(template_name='guest/pogoji_uporabe.html')


class lost_password(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response(template_name='guest/lost_password.html')

def randomStringDigits(stringLength=60):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))

class lost_password_verify(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, token):
        try:
            token = Pozadbljenogesloverifikacija.objects.get(token=token)
        except:
            return Response(template_name='guest/lost_password_unsuccess.html')

        usr = User.objects.get(id=token.user_id)
        naive = token.created_at.replace(tzinfo=None)
        seconds_from_request=datetime.datetime.now().timestamp()-naive.timestamp()

        if(seconds_from_request > 900):
            Pozadbljenogesloverifikacija.objects.filter(user_id=usr.id).delete()
            return Response(template_name='guest/lost_password_unsuccess.html')
        else:
            return Response({'token':token.token},template_name='guest/lost_password_success.html')

def merge_users(parentUser,childMails):
    for mail in childMails:
        try:
            childUser = User.objects.get(email=mail["Child_mail"])
        except:
            childUser = None

        if childUser.email != parentUser.email and childUser != None:
            childsUserSchools = Uci.objects.filter(ID_Mentor=childUser.id).values()
            childsUserCompetitions = Sodeluje.objects.filter(ID_Mentor=childUser.id).values()

            #preveži otrokove šole na očeta
            for school in childsUserSchools:
                try:
                    uciParent = Uci.objects.get(ID_Mentor=parentUser.id,ID_Sola=school["ID_Sola_id"])
                except Uci.DoesNotExist:
                    uciParent = None

                try:
                    uciChild = Uci.objects.get(ID_Mentor=childUser.id,ID_Sola=school["ID_Sola_id"])
                except Uci.DoesNotExist:
                    uciChild = None

                if uciChild != None:
                    if uciParent == None:
                        uciChild.ID_Mentor = parentUser
                        uciChild.save()
                        Potrditve.objects.filter(Zahteva_id=childUser.id,ID_Sola_potrditev_id=school["ID_Sola_id"]).update(Zahteva_id=parentUser.id)

                    if uciParent != None:
                        if uciParent.Potrjen == 0:
                            if uciChild.Potrjen == 1:
                                uciParent.Potrjen = 1
                                uciParent.save()
                                uciChild.delete()
                            else:
                                uciChild.delete()
                        else:
                            uciChild.delete()

                potrditveChild = Potrditve.objects.filter(Potrditelj_id=childUser.id,ID_Sola_potrditev_id=school["ID_Sola_id"]).values()
                for potrditev in potrditveChild:
                    # preveži otrokove potrditve na očeta.
                    # preveri ali ima oče že zapis, ki bi ga ob prevezavi imel,
                    # prav tako preveri, da oče ne potrjuje sam sebe
                    if not Potrditve.objects.filter(Potrditelj_id=parentUser.id,Zahteva_id=potrditev["Zahteva_id"],ID_Sola_potrditev_id=school["ID_Sola_id"]).exists():
                        if potrditev["Zahteva_id"] != parentUser.id:
                            Potrditve.objects.filter(Potrditelj_id=childUser.id,Zahteva_id=potrditev["Zahteva_id"],ID_Sola_potrditev_id=school["ID_Sola_id"]).update(Potrditelj_id=parentUser.id)
                        else:
                            Potrditve.objects.filter(Potrditelj_id=childUser.id,Zahteva_id=potrditev["Zahteva_id"],ID_Sola_potrditev_id=school["ID_Sola_id"]).delete()
                    else:
                        Potrditve.objects.filter(Potrditelj_id=childUser.id,Zahteva_id=potrditev["Zahteva_id"],ID_Sola_potrditev_id=school["ID_Sola_id"]).delete()

            # preveži otrokova tekmovanja na očeta
            for competition in childsUserCompetitions:
                if Sodeluje.objects.filter(ID_Mentor=parentUser.id,ID_Sola=competition["ID_Sola_id"],ID_Tekmovanje=competition["ID_Tekmovanje_id"]).exists():
                    Sodeluje.objects.filter(ID_Mentor=childUser.id,ID_Sola=competition["ID_Sola_id"],ID_Tekmovanje=competition["ID_Tekmovanje_id"]).delete()
                else:
                    Sodeluje.objects.filter(ID_Mentor=childUser.id,ID_Sola=competition["ID_Sola_id"],ID_Tekmovanje=competition["ID_Tekmovanje_id"]).update(ID_Mentor=parentUser.id)

            #pojdi čez otrokove zapise in jih pobriši, če obstajajo
            Potrditve.objects.filter(Potrditelj_id=childUser.id).delete()
            Potrditve.objects.filter(Zahteva_id=childUser.id).delete()
            email_merge.objects.filter(Child_mail=childUser.email).delete()
            email_merge.objects.filter(Master_mail=parentUser.email).delete()
            Verifikacija.objects.filter(user_id=childUser.id).delete()
            User.objects.filter(id=childUser.id).delete()


class merge_users_request(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, token):
        try:
            email = email_merge.objects.get(token=token)
        except:
            return Response(template_name='guest/merge/unsuccess.html')

        naive = email.created_at.replace(tzinfo=None)
        seconds_from_request=datetime.datetime.now().timestamp()-naive.timestamp()

        if(seconds_from_request > 21600):
            email_merge.objects.filter(token=email.token).delete()
            return Response(template_name='guest/merge/unsuccess.html')
        else:
            if email_merge.objects.filter(token=email.token,Potrjen=1).exists():
                return Response(template_name='guest/merge/already-confirmed.html')
            else:
                email_merge.objects.filter(token=email.token).update(Potrjen=1)

            master_mail=email_merge.objects.get(token=email.token).Master_mail
            childMails = email_merge.objects.filter(Master_mail=master_mail).values("Child_mail")

            if email_merge.objects.filter(Master_mail=master_mail).count() ==  email_merge.objects.filter(Master_mail=master_mail,Potrjen=1).count():
                #logika, ki preveže vse otroke na očeta
                try:
                    masterUser = User.objects.get(email=master_mail)
                except:
                    return Response(template_name='guest/merge/unsuccess.html')
                childMails = email_merge.objects.filter(Master_mail=master_mail).values("Child_mail")
                merge_users(masterUser,childMails)
                message="Pozdravljeni. Sporočamo Vam, da je bila združitev uporabniških računov v spletni aplikaciji ACM tekmovanja ("+project_settings.BASE_URL+") uspešna. Vsi računi so sedaj združeni v enega. Do računa lahko dostopate z uporabniškim imenom "+ masterUser.username +" in geslom, ki ga imate za ta račun."

                send_mail_to_user('ACM merge mails', message,[masterUser.email], False)

                return Response(template_name='guest/merge/success-all.html')
            else:
                return Response(template_name='guest/merge/success.html')

class all_schools(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a index page of user.
    AUTH REQUIRED
    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response(template_name='guest/all_schools.html')

