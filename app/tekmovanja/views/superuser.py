from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.shortcuts import redirect
from rest_framework import generics

#here are all templates for superuser

def checkSuperUserPermissions(user):
    return user.is_authenticated and user.is_superuser

class admin_bulk(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkSuperUserPermissions(request.user):
            return Response(template_name='superuser/bulk.html')
        else:
            return redirect('login')

class admin_merge(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        if checkSuperUserPermissions(request.user):
            return Response(template_name='superuser/merge.html')
        else:
            return redirect('login')